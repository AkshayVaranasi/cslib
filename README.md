# Structured Compressed Sensing, Imaging and Learning

Matlab code related to the upcoming book _Structured Compressed
Sensing, Imaging and Learning_ by Adcock and Hansen.  

## Code dependency
* [SPGL1](http://www.cs.ubc.ca/~mpf/spgl1/) A solver for large-scale sparse reconstruction
* [NESTA](http://statweb.stanford.edu/~candes/nesta/) A Fast and Accurate First-order Method for Sparse Recovery
* [ShearLab](http://www3.math.tu-berlin.de/numerik/www.shearlab.org/) 
    Shearlets (ShearLab3D v1.1)
* [CurveLab](ShearLab3D v1.1) Curvelets (CurveLab 2.1.3)
* [Fastwht](https://bitbucket.org/vegarant/fastwht/) A fast implementation of
  of matlabs `fwht`-function (Optional)
* [GS_walsh](https://bitbucket.org/vegarant/generalized-sampling-with-walsh-sampling) Generalized sampling with Walsh functions


## Install
1. To add the required directories to your matlab-path run the file `install_cslib.m`.
2. Run the file `etc/set_defaults.m` to create a file containing all the
default values required by most of the scripts.
3. Download all data files required and place them in the directory `data`. Link: https://drive.google.com/open?id=1BvK0-IAiHnR9PM6R59PdEGtIBiVpu6Fb 

## Note 
Many of the scripts change the dwt boundary extension mode in the current
session. 

