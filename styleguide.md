# Styleguide 

## General guide

- I divide the Matlab files into two categories, scripts and function files.
  A Matlab script is a file which is meant to create a plot, conduct
  an experiment or similar. A function file contains a function which 
  are used by one or more scripts. In Matlab 2016b and newer, one is 
  allowed to add local functions into scripts without creating a separate 
  file. To make the code backward compatible I do not use this feature. 

- The filename of any Matlab script should always start with a capital letter,
  whereas any function file should always start with a lower case letter. This 
  makes it easy to understand which type of file it is.

- For function and variable names I try to use lower_case_with_underscore. I try
  to avoid using camelCase. This is somewhat untypical in Matlab, but it makes 
  it easier whenever you use abbreviations in names. I guess there are other 
  pros and cons for both styles. 

- For functions I have chosen to put the documentation on top of the function,
  rather than on the first line below the function declaration. I guess this 
  is somewhat untypical. I also try to always include a list with a discription
  of the input an output arguments. Like
```
    % Some short sentence describing what the function does.
    %
    % INPUT
    % arg1 - Short description of first argument
    % arg2 - Short description of second argument
    % 
    % OUTPUT
    % out1 - Short description of the return value
```

- Never use Matlab classes. This feature is not very well supported in Matlab. 
  Classes makes it hard to debug the code, especially if you use super and 
  subclasses, since changes to a super class, might not have any effect before
  you have restarted Matlab. Lastly, all non-expert users will find it harder 
  to read the code, if we hide it in a "language" they do not understand.
  The only exception to this rule is whenever we write test code. Then we can 
  take advantage of Matlabs test suit. See src/testing 

- I try to add as few dependencies to the library as possible, so unless it 
  is absolutely necessary do not add any extra dependencies.  

- Do only commit files containing code. If we start committing images or other 
  types of data files into the git repository, the size of this repository 
  grows very fast, and it becomes hard to download. Git is not very well suited 
  for storing data, so we will place these in a separate location where people
  can download it. Right now I have it on google drive. I have committed a few
  image files, but do not commit any more. 

- Try to keep all lines shorter than 80 characters. 

## Sampling patterns

- All sampling patterns are located in the folder src/samp_patt.

- I have introduced the name convention spf2_name_of_pattern and
  sph2_name_of_pattern. Here 'sp' is a abbreviation for 'sampling pattern', 
  'f' and 'h' is a abbreviation for 'Fourier' and 'Hadamard', respectively, and 
  '2' indicates that it is a two dimensional pattern.

- All sampling pattern function return 'idx' and 'str_id'. Here idx are linear
  indices of the chosen samples and 'str_id' is a string identifier. The
  string identifier is strictly speaking not necessary, but I have found it
  very practical, when you run loops over different patterns and so on. 


