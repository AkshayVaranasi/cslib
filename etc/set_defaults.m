% This file set variuous parameters to get consistency between files
% The parameters found in this file, will be considered as default parameters

clear('all'); close('all');

% Create destination for the data
if (exist('../var') ~= 7) 
    mkdir('../var');
end

% cslib defaults
cslib_dflt.font_size = 14; 
cslib_dflt.marker_size = 8;
cslib_dflt.color =[0,    0.4470,    0.7410];% [0,0,0]; % Black
cslib_dflt.marker = 'o';
cslib_dflt.marker_edge_color = [0,    0.4470,    0.7410]; % Black
cslib_dflt.marker_face_color = [0,    0.4470,    0.7410]; % Black
cslib_dflt.cbar_line_width = 1.5;
cslib_dflt.line_width = 2;
cslib_dflt.math_font = 'Latin Modern Math';
cslib_dflt.image_format = 'png';
cslib_dflt.plot_format = 'epsc';
cslib_dflt.blue    = [0,    0.4470,    0.7410];
cslib_dflt.yellow  = [1,1,0]; 
cslib_dflt.green   = [102, 255, 51]./255;
cslib_dflt.brown   = [153, 102, 51]./255;
cslib_dflt.red     = [1, 0, 0];
cslib_dflt.magenta = [1, 0, 1];
cslib_dflt.cyan    = [0, 1, 1]; 
cslib_dflt.black   = [0, 0, 0];

cslib_dflt.line_color = 'k';
cslib_dflt.spgl1_verbose = 1;
cslib_dflt.verbose = 1;
cslib_dflt.data_path = '/home/vegant/db-cs/cslib_data';

cslib_dflt.cmap_matrix = jet(256);

save('../var/cslib_defaults.mat');


