clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

path_to_this_file = mfilename('fullpath');
path_to_this_file = path_to_this_file(1:end-length(mfilename)-1);
dest = fullfile(path_to_this_file, 'plots');

% Create destination for the plots
if (exist(dest) ~= 7) 
    mkdir(dest);
end

vm = 4;                                     % Number of vanishing moments
subsampling_rate = 0.06;                    % ∈ [0,1]

N = 1024;
imname = 'dog';
fname = sprintf('%s_%d.png',imname, N);
X = double(imread(fullfile(cslib_dflt.data_path, 'natural_images', fname)));
X = X/255;
cslib_dflt.image_format = 'png';

v = 206;
vs = 250;
hs = 550;
idx_v = vs:vs+v-1;
idx_h = hs:hs+v-1;


str_id = 'DAS';
fname1 = fullfile(cslib_dflt.data_path, 'natural_images', sprintf('%s_%d.png',imname, N));
fname2 = fullfile(dest, sprintf('aBernoulli_%s_db%d_wavelet.%s', imname, vm, cslib_dflt.image_format ));
fname3 = fullfile(dest, sprintf('aHadamard_%s_wavelet_srate_%d_db%d_%s.%s',imname, 100*subsampling_rate, vm, str_id, cslib_dflt.image_format ));
fname4 = fullfile(dest, sprintf('aHadamard_%s_shearlet_srate_%d_%s.%s', imname,100*subsampling_rate, str_id, cslib_dflt.image_format ));
fname5 = fullfile(dest, sprintf('aHadamard_%s_TV_srate_%d_%s.%s', imname, 100*subsampling_rate, str_id, cslib_dflt.image_format));

str_id = 'uniform';
fname6 = fullfile(dest, sprintf('aHadamard_%s_db%d_wavlet_%s.%s', imname, vm, str_id, cslib_dflt.image_format));
 
fnames = {fname1, fname3, fname4, fname5}; 
text = {'Original', 'Wavelet', 'Shearlet', 'TV'}; 

im = create_crop_image(fnames, text, idx_v, idx_h, cslib_dflt.font_size);
fname_out = sprintf('aHadamard_%s_srate_%d_%s_crop.%s', imname, 100*subsampling_rate, 'DAS', cslib_dflt.image_format);
imwrite(im, fullfile(dest, fname_out));
%imagesc(im); colormap('gray'); colorbar();



