% Figure 1.8
% Description: Perform a CS-experiment with various sampling binary sampling
% and sparsifying operators. 
% - Bernoulli & Wavelets
% - Hadamard & Wavelets DAS pattern
% - Hadamard & Wavelets uniformly random
%
% Vegard Antun 2017
%

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

path_to_this_file = mfilename('fullpath');
path_to_this_file = path_to_this_file(1:end-length(mfilename)-1);
dest = fullfile(path_to_this_file, 'plots');

% Create destination for the plots
if (exist(dest) ~= 7) 
    mkdir(dest);
end

disp_plots = 'off';

vm = 4;                                     % Number of vanishing moments
curvelet_is_real = 0;                       % 0 => Complex valued curvelets
shearlet_levels = 4;                        % Number of shearlet levels
subsampling_rate = 0.15;                    % ∈ [0,1]
sigma = 1;                                  % min ||x||_1 s.t. ||Ax-b||_2 ≤ sigma
sigma_tv = 0.1;

N = 256;
fname = sprintf('peppers1_%d.png', N);
X = double(imread(fullfile(cslib_dflt.data_path, 'natural_images', fname)));
X = X/255;

nbr_samples = round(subsampling_rate*N*N);

% Create sampling pattern
%idx = sph2_rect(N, nbr_samples);

sparsities = csl_compute_sparsity_of_image(phantom(N), vm); 
[idx, str_id] = sph2_DAS(N, nbr_samples, sparsities);

fname_desc = fullfile(dest, 'plot_description.txt');
fID =fopen(fname_desc, 'a'); % Write a description of each image to a file

% Bernoulli wavelet
max_iterations = 1000;
fprintf('Computing Bernoulli wavelet db%d reconstruction\n', vm);
fname = sprintf('aBernoulli_wavelet_srate_%d_db%d_maxItr_%d', 100*subsampling_rate, vm,max_iterations);
sample_bernoulli_wavelet(X, subsampling_rate, sigma, ... 
                         fullfile(dest, fname), vm, 'spgl1_iterations',max_iterations);
fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, sigma: %g\n', ...
                datestr(datetime('now')), fname, N, nbr_samples, sigma);

% Hadamard wavelet
fprintf('Computing Hadamard wavelet db%d reconstruction with %s\n', vm, str_id);
fname = sprintf('aHadamard_wavelet_srate_%d_db%d_%s', 100*subsampling_rate, vm, str_id);
sample_hadamard_wavelet(X, sigma, idx, fullfile(dest, fname), vm);
fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, sigma: %g\n', ...
                datestr(datetime('now')), fname, N, nbr_samples, sigma);

% Hadamard Wavelet uniformly random
[idx, str_id] = sp2_uniform(N, nbr_samples);
fprintf('Computing Hadamard wavelet db%d reconstruction with %s\n', vm, str_id);
fname = sprintf('aHadamard_wavelet_srate_%d_db%d_%s', 100*subsampling_rate, vm, str_id);
sample_hadamard_wavelet(X, sigma, idx, fullfile(dest, fname), vm);
fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, sigma: %g\n', ...
                datestr(datetime('now')), fname, N, nbr_samples, sigma);

fclose(fID);
 
 
 
 
 
 
 
 
 
 
 
 

 
 
 
