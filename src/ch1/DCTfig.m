% Description: Generates the discrete sine transform of a sparse vector and
% plots it
% 
% Ben Adcock, Vegard Antun 2016

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

path_to_this_file = mfilename('fullpath');
path_to_this_file = path_to_this_file(1:end-length(mfilename)-1);
dest = fullfile(path_to_this_file, 'plots');

% Create destination for the plots
if (exist(dest) ~= 7) 
    mkdir(dest);
end

disp_plots = 'off';

% Parameters
nu = 6; % 2^nu = signal size

% Dependent parameters
N = 2^(nu);

% construct sparse vector x
x = zeros(N,1);
supp = [12 ; 16 ; 32 ; 40 ; 128 ; 200 ]/4;
x(supp) = randn(size(supp));

% plot x
fig1 = figure('Visible', disp_plots);
ms = 8;
lw = 2;
t = x;
t(x==0) = NaN;
stem(t, 'Marker', cslib_dflt.marker, ...
        'MarkerSize',cslib_dflt.marker_size,...
        'MarkerEdgeColor', cslib_dflt.marker_edge_color, ...
        'Color', cslib_dflt.color, ...
        'LineWidth', cslib_dflt.line_width);

xlim([1,N+1]);
ymax = max(abs(x))*1.1;
ylim([-ymax,ymax]);

saveas(fig1, fullfile(dest, 'sparse_x'), cslib_dflt.plot_format);

% plot sinusoid
fig2 = figure('Visible', disp_plots);

y = dct(x)/sqrt(N);
stem(y, 'Marker', cslib_dflt.marker, ...
        'MarkerSize',cslib_dflt.marker_size,...
        'MarkerEdgeColor', cslib_dflt.marker_edge_color, ...
        'Color', cslib_dflt.color, ...
        'LineWidth', cslib_dflt.line_width);

xlim([1,N+1]);
ymax = max(abs(y))*1.1;
ylim([-ymax,ymax]);

saveas(fig2, fullfile(dest, 'sparse_cos'), cslib_dflt.plot_format);




