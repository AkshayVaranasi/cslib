% Description: Perform a CS-experiment with various sampling fourier sampling
% and sparsifying operators. 
% - Gauss & Wavelets
% - Fourier & Wavelets
% - Fourier & Curvelets
% - Fourier & Shearlets
% - Fourier & TV
%
% Vegard Antun 2017
%

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

path_to_this_file = mfilename('fullpath');
path_to_this_file = path_to_this_file(1:end-length(mfilename)-1);
dest = fullfile(path_to_this_file, 'plots');

% Create destination for the plots
if (exist(dest) ~= 7) 
    mkdir(dest);
end

disp_plots = 'off';

vm = 4;                                     % Number of vanishing moments
curvelet_is_real = 0;                       % 0 => Complex valued curvelets
shearlet_levels = 4;                        % Number of shearlet levels
subsampling_rate = 0.20;                    % ∈ [0,1]
sigma = 1;                                  % min ||x||_1 s.t. ||Ax-b||_2 ≤ sigma
sigma_tv = 0.1;

N = 256;
fname = sprintf('brain1_%d.png', N);
X = double(imread(fullfile(cslib_dflt.data_path, 'MR', fname)));

nbr_samples = round(subsampling_rate*N*N);

% Create sampling pattern
sparsities = csl_compute_sparsity_of_image(phantom(N), vm); 
%[idx, str_id] = spf2_DAS(N, nbr_samples, sparsities, vm);

fname_desc = fullfile(dest, 'plot_description.txt');
fID =fopen(fname_desc, 'a'); % Write a description of each image to a file

fully_sample = 0.100;
nbr_levels = 30;
p_norm = 2;

[idx, str_id] = spf2_exp(N, nbr_samples, fully_sample, nbr_levels, p_norm); 
length(idx)
% Fourier wavelet
fprintf('Computing wavelet reconstruction\n');
fname = sprintf('aFourier_wavelet_srate_%g_db%d_%s', subsampling_rate*100, vm, str_id);
sample_fourier_wavelet(X, sigma, idx, fullfile(dest, fname), vm);
fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, sigma: %g\n', ...
                datestr(datetime('now')), fname, N, nbr_samples, sigma);


nbr_lines = 21
all_samples = 1;
[idx, str_id] = spf2_lines(N, nbr_samples, nbr_lines, all_samples); 
length(idx)

%Z = zeros([N,N]);
%Z(idx) = 1;
%imagesc(Z);
%colormap('gray');

% Fourier wavelet
fprintf('Computing wavelet reconstruction\n');
fname = sprintf('aFourier_wavelet_srate_%g_db%d_%s', subsampling_rate*100, vm, str_id);
sample_fourier_wavelet(X, sigma, idx, fullfile(dest, fname), vm);
fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, sigma: %g\n', ...
                datestr(datetime('now')), fname, N, nbr_samples, sigma);




% Gauss wavelet
fname = sprintf('aFGauss_srate_%g_db%d_wavelet', subsampling_rate*100, vm);
sample_gauss_wavelet(X, subsampling_rate, sigma, ... 
                         fullfile(dest, fname), vm);
fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, sigma: %g\n', ...
                datestr(datetime('now')), fname, N, nbr_samples, sigma);

%%%% Fourier Wavelet uniformly random
%%%[idx, str_id] = sp2_uniform(N, nbr_samples);
%%%fname = sprintf('aFourier_srate_%g_db%d_wavlet_%s', subsampling_rate*100, vm, str_id);
%%%sample_fourier_wavelet(X, sigma, idx, fullfile(dest, fname), vm);
%%%fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, sigma: %g\n', ...
%%%                datestr(datetime('now')), fname, N, nbr_samples, sigma);


fclose(fID);


