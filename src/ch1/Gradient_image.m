% Description: Computes the gradient image of the boat image
% Vegard Antun 2016

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

path_to_this_file = mfilename('fullpath');
path_to_this_file = path_to_this_file(1:end-length(mfilename)-1);
dest = fullfile(path_to_this_file, 'plots');

% Create destination for the plots
if (exist(dest) ~= 7) 
    mkdir(dest);
end

disp_plots = 'off';

im = imread('cslib_boat.tiff'); % load image
X = im2double(im); 

H = fspecial('sobel');

Y1 = imfilter(X,H, 'symmetric');
Y2 = imfilter(X,H', 'symmetric');

Y = max(abs(Y1), abs(Y2));
fig = figure('Visible', disp_plots);
imagesc(abs(Y1));
colormap('gray');
colorbar();

fname = sprintf('boat_gradient.%s',cslib_dflt.image_format); 
imwrite(im2uint8(Y), fullfile(dest, fname)); 
fname = sprintf('boat.%s',cslib_dflt.image_format); 
imwrite(im, fullfile(dest, fname)); 

