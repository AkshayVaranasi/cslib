% Script producing the wavelet coefficients image and an image where some
% percent of coefficients have been from a source image
% Edvard Aksnes July 2017

clear('all'); close('all');
load('cslib_defaults')

path_to_this_file = mfilename('fullpath');
path_to_this_file = path_to_this_file(1:end-length(mfilename)-1);
dest = fullfile(path_to_this_file, 'plots');

% Create destination for the plots
if (exist(dest) ~= 7) 
    mkdir(dest);
end

disp_plots = 'off';
dwtmode('per', 'nodisp');

wname='db2';

% reading the chosen image
im = double(imread('cslib_pomegranate.jpg'));

keep_fraction=0.01; % percent of wavelet coefficients to be retained
% recreating it using some percent of coefficients
A = csl_wavelet_sparsify2d(im, keep_fraction, wname);

% making a plot of the coefficients form the DWT
N = wmaxlev(size(im), wname);
[c,s] = wavedec2(im,N,wname);
O = csl_wave_ord_2d_image(c, s, wname);


O = (O - min(abs(O(:))))/(max(abs(O(:))) - min(abs(O(:))));
A = (A - min(A(:)))/(max(A(:)) - min(A(:)));
O=(abs(O)).^(0.21);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fname_desc = fullfile(dest, 'plot_description.txt');
fID =fopen(fname_desc, 'a'); % Write a description of each image to a file

fname = sprintf('wavelet_coef.%s' ,cslib_dflt.image_format);
imwrite(im2uint8(O),fullfile(dest, fname) );
fprintf(fID, '%s: %s, %s\n', ...
                datestr(datetime('now')), fname, wname);

fname = sprintf('sparsified.%s' ,cslib_dflt.image_format);
imwrite(im2uint8(A), fullfile(dest, fname) );
fprintf(fID, '%s: %s, %s, kept %g%% of the wave coef.\n', ...
                datestr(datetime('now')), fname, wname, 100*keep_fraction);


fclose(fID);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




