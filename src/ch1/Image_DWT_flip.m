% Description: computes the DWT of an image, randomly permutes the
% coefficients and then computes the inverse DWT
% 
% Ben Adcock, Vegard Antun 2016

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

path_to_this_file = mfilename('fullpath');
path_to_this_file = path_to_this_file(1:end-length(mfilename)-1);
dest = fullfile(path_to_this_file, 'plots');

% Create destination for the plots
if (exist(dest) ~= 7) 
    mkdir(dest);
end

disp_plots = 'off';
dwtmode('per', 'nodisp');

wave_name = 'db4'; % Using a db4 wavelet
percent = 10; % Number of wavelet coefficents


im = imread('../../data/cslib_donkey.jpg'); % load image
im = im2double(im); 
[n, m] = size(im);
s = round(percent*n*m/100);

lx = 100;
im = im(:,lx:lx+n-1);
[n, m] = size(im);

[im_sparse, wave_coef,S] = csl_wavelet_sparsify2d(im, percent/100, wave_name);

fname_desc = fullfile(dest, 'plot_description.txt');
fID =fopen(fname_desc, 'a'); % Write a description of each image to a file




% plot im_sparse
fname = 'sparse_donkey';
idx = im_sparse > 1;
im_sparse(idx) = 1;
imwrite(im2uint8(im_sparse), fullfile(dest, [fname,'.', cslib_dflt.image_format]))

fprintf(fID, '%s: %s, %s, Using %g%% of the wavelet coeffs\n', ...
                datestr(datetime('now')), fname, wave_name, percent);


% randomly permute sparse wavelet coefficients
wave_coef_perm = flip_wavelet2_in_levels(wave_coef, S);
%wave_coef(randperm(n*m));

% compute permuted image
im_sparse_perm = waverec2(wave_coef_perm, S, wave_name);
im_out = scale_to_01(im_sparse_perm);
fname = 'sparse_permuted_donkey';
imwrite(im2uint8(im_out), fullfile(dest,[fname,'.', cslib_dflt.image_format]))


fprintf(fID, '%s: %s, %s, Using %g%% of the wavelet coeffs\n', ...
                datestr(datetime('now')), fname, wave_name, percent);


fclose(fID);


