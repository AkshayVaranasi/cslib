% This script demonstrates the Turbo AMP algorithm reconstruction with
% Bernoulli measurements.
%
% Edvard Aksnes July 2017

close all; clear all; clear java;
load('cslib_defaults')

if (exist('plots') ~= 7)
    mkdir('plots');
end
dest = 'plots/';
disp_plots = 'off';
dwtmode('per', 'nodisp');


% read image file 
im = imread('../../data/peppers.tiff');
X = rgb2gray(im);
X = im2double(imresize(X,0.5)); % make the image smaller for more manageable

percent=0.125;
M=round((prod(size(X)))*percent);             % number of measurements

b=addpath_hmt_struct();

if b==false;
  return
end

% turning this warning off to suppress warning from turboAMP about adding
% a java class
warning('off','MATLAB:javaclasspath:invalidFile')

% wavelet tree depth
numLevels = wmaxlev(size(X,1),'db1');

%%% settings for turbo AMP algorithm %%%
params = getConfigTurboAMP();
params.projOnImage=1;
params.numLevels=numLevels;
params.s=get_wavedec2_s(round(log2(size(X,1))), numLevels);

% Create a Bernuolli sesing matrix A. If the current machine is not able to 
% store the matrix A in memory, size of X and M will be reduced so that it is
% able to continue the calculations. 
[A,X,M] = generateBernoulliMatrix(X,M);

% Measurement vector 
y = A*X(:);

[x_hat, s_hat]=turboAMP(y,A,[],params,1);

warning('on','MATLAB:javaclasspath:invalidFile')


% Writing the recovered image to file.
imwrite(im2uint8(X),sprintf('%sTurboAMP_original.%s',dest,cslib_dflt.image_format));
imwrite(im2uint8(reshape(x_hat,size(X))),sprintf('%sTurboAMP_reconstruction.%s',dest,cslib_dflt.image_format));


