classdef Test_change_of_basis < matlab.unittest.TestCase
    % Test_samp_patt test the various sampling patterns so that all of them
    % produces the right amount of samples and no sample is sampled twice. 
    %
    % USAGE:
    % >> testCase = Test_samp_patt; 
    % >> res = run(testCase)
    %
    properties
        N;   % Create N×N patterns
        eps;
    end  
    
    methods (Test)
        % Constructor
        function obj = Test_change_of_basis(testCase)
            obj.N   = 2^9;
            obj.eps = 1e-7;
        end
   
        function obj = test_hadIDB2_unitary(testCase)
            N = testCase.N;
            eps = testCase.eps;
            vm = 4;
            idx = 1:N^2;
            nres = 3;
            dwtmode('per', 'nodisp');
            wave_name = sprintf('db%d', vm);
            had_order = 'sequency';
            A = @(x, mode) had2IDB2d(x, mode, N, idx, nres, wave_name, ...
                                     had_order);
           
            X = rand([N,N]);
            Y = A(A(X, 0),1);
            Y = reshape(Y, [N,N]);
            success = norm(X-Y) < eps;
            
            Z = A(A(X, 1),0);
            Z = reshape(Z, [N,N]);
            success = success & norm(X-Z) < eps;
            
            testCase.verifyTrue(success);
            
        end 
        
    end
    methods (Access=private)
    end
end 




