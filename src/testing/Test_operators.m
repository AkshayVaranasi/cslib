classdef Test_operators < matlab.unittest.TestCase
    % Test_operators tests whether the fourier2op is linear and that AA'= I.
    %
    % USAGE:
    % >> testCase = Test_operators; 
    % >> res = run(testCase)
    %
    properties
        N;   % Create N×N patterns
        eps;
    end  
    
    methods (Test)
        % Constructor
        function obj = Test_operators(testCase)
            obj.N   = 2^7;
            obj.eps = 1e-10;
        end
    
        
        function obj = test_fourier2op_linear(testCase)

            N = testCase.N;
            eps = testCase.eps;
            idx = 1:2:N*N;

            x = ones(N*N,1);
            y = rand(N*N,1);
            
            a = 100*rand(1);
            b = 100*rand(1);
            f = @(t) fourier2op(t, 1,N,idx);
            
            A = f(a*x - b*y);
            B = a*f(x) - b*f(y);
            success = norm(A-B) < eps;
            testCase.verifyTrue(success);
            
            x = ones(numel(idx),1);
            y = rand(numel(idx),1);
            
            g = @(t) fourier2op(t, 0,N,idx);
            
            A = g(a*x - b*y);
            B = a*g(x) - b*g(y);
            success = norm(A-B) < eps;
            testCase.verifyTrue(success);
        end
        
        function obj = test_orthonormal_rows(testCase)

            N = testCase.N;
            eps = testCase.eps;
            idx = 1:2:N*N;

            y = rand(numel(idx),1);
            
            backward_fds = @(t) fourier2op(t, 0,N,idx);
            forward_fds  = @(t) fourier2op(t, 1,N,idx);
            
            x = forward_fds(backward_fds(y));
            success = norm(y-x) < eps;
            testCase.verifyTrue(success);
            
        end
    end
    methods (Access=private)
    end
end 



