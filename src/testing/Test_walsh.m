classdef Test_walsh < matlab.unittest.TestCase
    %
    % USAGE:
    % >> testCase = Test_walsh; 
    % >> res = run(testCase)
    %
    properties
        N;   % Create N×N patterns
        eps;
    end  
    
    methods (Test)
        % Constructor
        function obj = Test_walsh(testCase)
            obj.N   = 2^9;
            obj.eps = 1e-10;
        end
    
        
        function obj = test_fastwht2_linear(testCase)

            N = testCase.N;
            eps = testCase.eps;

            X = rand([N,N]);
            Y = fastwht2(X);

            a = 100*rand(1);
            b = 100*rand(1);

            A = fastwht2(a*X - b*Y);
            B = a*fastwht2(X) - b*fastwht2(Y);
            success = norm(A-B) < eps;
            testCase.verifyTrue(success);

        end
        
        function obj = test_unitary(testCase)

            N = testCase.N;
            eps = testCase.eps;

            X = rand([N,N]);
            Y = fastwht2(fastwht2(X));

            success = norm(X-Y) < eps;
            testCase.verifyTrue(success);

        end
        
        function obj = Test_fastwht2(testCase)    
            
            N = testCase.N;
            eps = testCase.eps;
            
            X = rand([N,N]);
            Y = fastwht(fastwht(X)')'*N;
            Z = fwht(fwht(X)')'*N;
            %fprintf('\n\n||Y-Z||: %g \n\n', norm(Y-Z));
            success = norm(Y-Z) < eps;
            testCase.verifyTrue(success);
            
        end
    end
    methods (Access=private)
    end
end 



