classdef Test_samp_patt < matlab.unittest.TestCase
    % Test_samp_patt test the various sampling patterns so that all of them
    % produces the right amount of samples and no sample is sampled twice. 
    %
    % USAGE:
    % >> testCase = Test_samp_patt; 
    % >> res = run(testCase)
    %
    properties
        N;   % Create N×N patterns
        nu;  % N = 2^nu 
        eps; % Error threshould
        subsampling_rate;
        M;   % = round(N*N*subsampling_rate)
    end  
    
    methods (Test)
        % Constructor
        function obj = Test_samp_patt(testCase)
            obj.nu = 9;
            obj.N = 2^obj.nu;
            obj.eps = 1e-8;
            obj.subsampling_rate = 0.125;
            obj.M = round(obj.N*obj.N*obj.subsampling_rate);
        end
        
        
        function test_sp2_uniform(testCase)
            N   = testCase.N;
            M   = testCase.M;
            idx = sp2_uniform(N, M);
            test_pattern(testCase, idx);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%                  Hadamard sampling patterns                     %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        function test_sph2_rect(testCase)
            
            N   = testCase.N;
            M   = testCase.M;
            idx = sph2_rect(N, M);
            
            test_pattern(testCase, idx);
        end
        
        
        function test_sph2_gauss(testCase)
            
            nu  = testCase.nu;
            N   = testCase.N;
            M   = testCase.M;
            idx = sph2_gauss(N, M, round(nu/3));
            
            test_pattern(testCase, idx);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%                   Fourier sampling patterns                     %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        function test_spf2_lines(testCase)
            % Note this function will fail if one insert the wrong number of
            % lines
            N   = testCase.N;
            M   = testCase.M;
            idx = spf2_lines(N, M, 15, 1);
            
            test_pattern(testCase, idx);
        end
        
        function test_spf2_square(testCase)
            nu  = testCase.nu;
            N   = testCase.N;
            M   = testCase.M;
            bounds = [2^(nu-4), 2^(nu-2), 2^(nu-1)]; % Radius of each sampling level 
            idx = spf2_square(N, M, bounds);
            test_pattern(testCase, idx);
        end
        
        

        function test_spf2_map(testCase)
            
            N   = testCase.N;
            M   = testCase.M;
            f = @(x) 1 - x.^(0.20); 
            idx = spf2_map(N, M, f);
            
            test_pattern(testCase, idx);
        end
        
        function test_spf2_circle(testCase)
            nu  = testCase.nu;
            N   = testCase.N;
            M   = testCase.M;
            bounds = [2^(nu-4), 2^(nu-2), 2^(nu-1)]; % Radius of each sampling level 
            idx = spf2_circle(N, M, bounds, 1, 0);
            test_pattern(testCase, idx);
        end
        
        function test_spf2_circle_with_border(testCase)
            nu  = testCase.nu;
            N   = testCase.N;
            M   = testCase.M;
            bounds = [2^(nu-4), 2^(nu-2), 2^(nu-1)]; % Radius of each sampling level 
            idx = spf2_circle(N, M, bounds, 1, 1);
            test_pattern(testCase, idx);
        end
    end
    methods (Access=private)
        % Tests that idx contains the right amount of samples and that none of
        % the samples are sampled more than once 
        function test_pattern(testCase, idx)
            M = testCase.M;
            N = testCase.N;
            success = (size(idx, 1) == M);
            
            Y = zeros([N,N], 'uint8');
            Y(idx) = 1;
            s = sum(Y(:));
            
            success = ((s == M) & success);
            
            testCase.verifyTrue(success);
            
        end
    end
end 


