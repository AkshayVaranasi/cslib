clear all;

% Variables
dim = 9;                                    % Image of size 2^dim × 2^dim
vm = 2;                                     % Number of vanishing moments
subsampling_rate = 0.10;                    % ∈ [0,1]
sigma = 1;                                  % min ||x||_1 s.t. ||Ax-b||_2 ≤ sigma
sigma_tv = 0.1;
dest = '../plots/';                         % Destination of the output images
                                            % (only the path, not the entire
                                            % filename) 

% Initialization
N = 2^dim;
M = round(subsampling_rate*(2^(dim*2)));    % Number of samples 
im = phantom(N);                            % Create image
bounds = [2^(dim-4), 2^(dim-2), 2^(dim-1)]; % Radius of each sampling level 
lines = 22;                                 % Number of lines in the radial
                                            % sampling plot

if (~exist(dest))
    mkdir(dest)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%              Create sampling map and reconstruction plots                %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic


% Sample in circels
idx = spf2_circle(N, M, bounds, 1, 0);
sample_fourier_wavelet(im, sigma, idx, strcat(dest, 'FourierWave1'), vm);
%sample_fourier_TV(im, sigma, idx, strcat(dest, 'FourierTV1'));
fprintf('Circle 1 complete\n');

% Sample in circles and include the corners
idx = spf2_circle(N, M, bounds, 1, 1);
sample_fourier_wavelet(im, sigma, idx, strcat(dest, 'FourierWave2'), vm);
%sample_fourier_TV(im, sigma, idx, strcat(dest, 'FourierTV2'));
fprintf('Circle 2 complete\n');

% Sample in tree squares 
idx = spf2_square(N, M, bounds);
sample_fourier_wavelet(im, sigma, idx, strcat(dest, 'FourierWave3'), vm);
sample_fourier_TV(im, sigma, idx, strcat(dest, 'FourierTV3'));
fprintf('Square 1 complete\n');

% Sample in two squares 
idx = spf2_square(N, M, [2^(dim-4), 2^(dim-1)]);
sample_fourier_wavelet(im, sigma, idx, strcat(dest, 'FourierWave4'), vm);
sample_fourier_TV(im, sigma, idx, strcat(dest, 'FourierTV4'));
fprintf('Square 2 complete\n');

% Sample uniformly at random
idx = spf2_uniform(N, M);
sample_fourier_wavelet(im, sigma, idx, strcat(dest, 'FourierWave5'), vm);
sample_fourier_TV(im, sigma, idx, strcat(dest, 'FourierTV5'));
fprintf('Uniform complete\n');

% Sample along radial lines random
idx = spf2_lines(N, M, lines);
sample_fourier_wavelet(im, sigma, idx, strcat(dest, 'FourierWave6'), vm);
sample_fourier_TV(im, sigma, idx, strcat(dest, 'FourierTV6'));
fprintf('Radial lines complete\n');

% Sample using a point cloud
f = @(x) 1 - x.^(0.20); 
idx = spf2_map(N, M, f);
sample_fourier_wavelet(im, sigma, idx, strcat(dest, 'FourierWave7'), vm);
%sample_fourier_TV(im, sigma, idx, strcat(dest, 'FourierTV7'));
fprintf('Point cloud complete\n');



toc


