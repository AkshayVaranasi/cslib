% For each level in a wavelet decomposition this script measures the local
% sparsity within the level, with respect to some epsilon ∈ [0,1]. 
% For each level it then plots the sparsity ratio agains epsilon. 
%
% Vegard Antun, 2018
%
clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% Create destination for the plots
if (exist('plots') ~= 7) 
    mkdir('plots');
end

dest = 'plots/';
disp_plots = 'on';
dwtmode('per', 'nodisp');

% Parameters
nu = 9;      % 2^nu = signal size
wname = 'Haar';
nres = nu-2;
N_eps = 101; % Number of epsilon values

% Dependent parameters
N = 2^(nu);                     % N × N image
%fname = sprintf('../../data/klubbe_%d.png', N);
fname = sprintf('/home/akshay/Desktop/CVC/Multi_target/Code/Multi-level_Sampling/tubule_512.png');
eps = linspace(0,1,N_eps); 
im = imread(fname);

[c,S] = wavedec2(im, nres, wname);

W = zeros(nres, N_eps);

for i = 1:nres
    [H1,V1,D1] = detcoef2('all',c,S,i);
    A = [H1, V1, D1];
    A = A(:);
    M = length(A);

    for j = 1:N_eps
        L = local_sparsity(A, eps(j));
        W(i,j) = L/M;
    end
end

label = cell(1,nres);
fig = figure('Visible', disp_plots);
for i = 1:nres
    plot(eps, W(i,:), 'LineWidth', cslib_dflt.line_width);
    hold('on');
    label{i} = sprintf('W_%d', nres -i+1);
end
legend(label, 'Fontsize', cslib_dflt.font_size, ...
              'Location', 'northwest');
xlabel('\epsilon', 'FontSize', cslib_dflt.font_size);
ylabel('S(\epsilon)/(M_{l} - M_{l-1})', 'FontSize', cslib_dflt.font_size);
set(gca,'fontsize', cslib_dflt.font_size)

fname = sprintf('%slocal_sparsity.%s', dest, cslib_dflt.plot_format);
saveas(fig, fname);


