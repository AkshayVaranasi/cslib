% This script demonstrate the flip test and flip test in levels 
% for gaussian sampling and wavelet reconstruction. Change the variable 
% flip_test_in_levels, to choose the which of the tests you would like to run.
%
% Vegard Antun, 2018

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% Create destination for the plots
if (exist('plots') ~= 7) 
    mkdir('plots');
end

dest = 'plots/';
dwtmode('per', 'nodisp');

% Parameters
nu = 8;                        % 2^nu = signal size
subsampling_rate = 0.20;       % 
vanishing_moments = [4, 6];    % The experiment is preformed for each number 
                               % specified 
sigma = 0;                     % Noise parameter

flipp_test_in_levels = 1;% Boolean, 0: flippes all wavelet coefficents
                         %          1: flippes wavelts coefficents within each
                         %          level

% Dependent parameters
N = 2^(nu);                                % N × N image
nbr_samples = round(N*N*subsampling_rate); % Number of samples
fname = sprintf('../../data/klubbe_%d.png', N);
fname_desc = sprintf('%splot_description.txt', dest);
im = double(imread(fname)); % OBS: image must have range [0,255] for PSNR calculations


for vm = vanishing_moments

    wname = sprintf('db%d', vm);    % Wavelet name 
    nres = wmaxlev([N,N], wname);

    % Create an image where all wavelet coefficients are flipped.
    [c,S] = wavedec2(im, nres, wname);
    if (flipp_test_in_levels)
        c_flipped = flip_wavelet2_in_levels(c, S);
        test_str_id = 'FTL'; % Flip test in levels
    else 
        c_flipped = flip(c);
        test_str_id = 'FT'; % Flip test
    end
    
    im_flipped = waverec2(c_flipped, S, wname);
    
    
    fID =fopen(fname_desc, 'a'); % Write a description of each image to a file
    
    % Unflipped
    fname = sprintf('%s%s_gauss_wave_db%d_unflipped', dest, test_str_id, vm);
    [im_rec, wave_rec] = sample_gauss_wavelet(im, subsampling_rate,  sigma,...
                                              fname, vm);
    
    [peaksnr, snr] = psnr(255*im_rec, im, 255); 
    
    fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, subsampling_rate: %g, db%d, QCBP noise: %g, PSNR: %g, SNR:%g\n', ...
            datestr(datetime('now')), fname, N, nbr_samples,   ...
            subsampling_rate, vm, sigma, peaksnr, snr);
    
    
    % Flipped
    fname = sprintf('%s%s_gauss_wave_db%d_flipped', dest, test_str_id, vm);
    [im_rec, wave_rec_flipped] = sample_gauss_wavelet(im_flipped, ...
                                 subsampling_rate, sigma, fname, vm);
    
    % Flip the wavelet coefficients back again, and recover image 
    if (flipp_test_in_levels)
        wave_rec_unflipped = flip_wavelet2_in_levels(wave_rec_flipped, S);
    else
        wave_rec_unflipped = flip(wave_rec_flipped);
    end
    
    im_rec_unflipped = waverec2(wave_rec_unflipped, S, wname);
    
    im_rec_unflipped = (im_rec_unflipped - min(im_rec_unflipped(:)))/...
                       (max(im_rec_unflipped(:)) - min(im_rec_unflipped(:)));
    
    im_rec_unflipped = abs(im_rec_unflipped);
    
    [peaksnr, snr] = psnr(255*im_rec_unflipped, im, 255); 
    
    fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, subsampling_rate: %g, db%d, QCBP noise: %g, PSNR: %g, SNR:%g\n', ...
            datestr(datetime('now')), fname, N, nbr_samples,   ...
            subsampling_rate, vm, sigma, peaksnr, snr);
    
    % Store the recovered image
    imwrite(im2uint8(im_rec_unflipped), sprintf('%s.%s', fname, ...
                                                cslib_dflt.image_format));

end

fclose(fID);





