% This script demonstrate the flip test with random permutations 
% for Fourier sampling and wavelet reconstruction. 
%
% Vegard Antun 2018

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% Create destination for the plots
if (exist('plots') ~= 7) 
    mkdir('plots');
end

dest = 'plots/';
dwtmode('per', 'nodisp');

% Parameters
nu = 8;                        % 2^nu = signal size
subsampling_rate = 0.20;
sigma = 0;                     % Noise parameter
vm = 1;

nbr_sampling_levels = 15;
% Dependent parameters
N = 2^(nu);                     % N × N image
nbr_samples = round(N*N*subsampling_rate); % Number of samples
nres = nu-3;%wmaxlev([N,N], wname);
%fname = sprintf('../../data/klubbe_%d.png', N);
fname_desc = sprintf('%splot_description.txt', dest);
im = 255*phantom(N); % double(imread(fname)); % OBS: image must have range [0,255] for PSNR calculations

wname = sprintf('db%d', vm);

% Create an image where all wavelet coefficients are flipped.
idx_perm1 = randperm(N*N);
idx_perm2 = randperm(N*N);

S = get_wavedec2_s(nu, nres);
idx_perm3 = csl_wave_ord_2d_image(1:N*N, S, wname);
idx_perm3 = flipud(fliplr(idx_perm3));
idx_perm3 = csl_wave_ord_2d_leveled(idx_perm3, S);


test_str_id = 'PT'; % Flip test in levels

[c,S] = wavedec2(im, nres, wname);
% Create sampling pattern
[idx, samp_str_id] = spf2_exp(N, nbr_samples, 0.01, nbr_sampling_levels, 2); 


eps = 0.6;
sparsity = sum(abs(c)>eps);
ma = max(im(:));
mi = min(im(:));


fID =fopen(fname_desc, 'a'); % Write a description of each image to a file

fprintf(fID, '%s: Permute test: sum(abs(wave_coeff) > %d) = %d, image range: [%g, %g], eps/range: %g\n', ... 
            datestr(datetime('now')), eps, sparsity, mi,ma, eps/(ma-mi));

idx_perm_all = {idx_perm1, idx_perm2, idx_perm3};
idx_perm_str_all = {'rand1','rand2', 'updown'};
for i = 1:length(idx_perm_all)
    idx_perm = idx_perm_all{i};
    idx_perm_inv = zeros(1,N*N);
    idx_perm_inv(idx_perm) = 1:N*N;
    idx_perm_str = idx_perm_str_all{i};

    c_permuted = c(idx_perm);
    assert(norm(c - c_permuted(idx_perm_inv)) < 1e-8)
    % Visualize the original wavelet coefficents and the permuted once
    c_im = abs(csl_wave_ord_2d_image(c, S, wname));
    c_im_permuted = abs(csl_wave_ord_2d_image(c_permuted, S, wname));
            
    c_im = (c_im - min(c_im(:)))/(max(c_im(:))-min(c_im(:)));
    c_im_permuted = (c_im_permuted - min(c_im_permuted(:)))/...
                    (max(c_im_permuted(:))-min(c_im_permuted(:)));
    
    fname = sprintf('%s%s_wavecoef_db%d_%s_no_perm', dest, test_str_id, vm, idx_perm_str);
    % Save the recovered image
    imwrite(im2uint8(c_im.^(1/3)), sprintf('%s.%s', fname, ...
                                                cslib_dflt.image_format));
    
    fname = sprintf('%s%s_wavecoef_db%d_%s_perm', dest, test_str_id, vm, idx_perm_str);
    % Save the recovered image
    imwrite(im2uint8(c_im_permuted.^(1/3)), sprintf('%s.%s', fname, ...
                                                cslib_dflt.image_format));
    
    % Save the unnatural image from permuted wavelet coefficents
    im_permuted = waverec2(c_permuted, S, wname);
    im_permuted_scaled = (im_permuted - min(im_permuted(:)))/...
                    (max(im_permuted(:))-min(im_permuted(:)));

    fname = sprintf('%s%s_image_perm_db%d_%s', dest, test_str_id,vm, idx_perm_str);
    imwrite(im2uint8(im_permuted_scaled), sprintf('%s.%s', fname, ...
                                                cslib_dflt.image_format));
    
    fname = sprintf('%s%s_four_wave_%s_db%d_%s_no_perm',dest, test_str_id, samp_str_id, vm, idx_perm_str);
    [im_rec, wave_rec] = sample_fourier_wavelet(im, sigma, idx, fname,...
                                                vm, 'wave_levels', nres);
    [peaksnr, snr] = psnr(255*im_rec, im, 255); 
    
    fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, subsampling_rate: %g, db%d, QCBP noise: %g, PSNR: %g, SNR:%g\n', ...
            datestr(datetime('now')), fname, N, nbr_samples,   ...
            subsampling_rate, vm, sigma, peaksnr, snr);
    
    
    fname = sprintf('%s%s_four_wave_%s_db%d_%s_perm', dest, test_str_id, ...
                    samp_str_id, vm, idx_perm_str);
    [im_rec, wave_rec_permuted] = sample_fourier_wavelet(im_permuted, ...
                                 sigma, idx, fname, vm);
    
    % Flip the wavelet coefficients back again, and recover image 
    wave_rec_inv_perm = wave_rec_permuted(idx_perm_inv);

    im_rec_inv_perm = waverec2(wave_rec_inv_perm, S, wname);
    
    im_rec_inv_perm = (im_rec_inv_perm - min(im_rec_inv_perm(:)))/...
                       (max(im_rec_inv_perm(:)) - min(im_rec_inv_perm(:)));
    
    im_rec_inv_perm = abs(im_rec_inv_perm);
    
    [peaksnr, snr] = psnr(255*im_rec_inv_perm, im, 255); 
    
    fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, subsampling_rate: %g, db%d, QCBP noise: %g, PSNR: %g, SNR:%g\n', ...
            datestr(datetime('now')), fname, N, nbr_samples,   ...
            subsampling_rate, vm, sigma, peaksnr, snr);
    
    % Save the recovered image
    imwrite(im2uint8(im_rec_inv_perm), sprintf('%s.%s', fname, ...
                                                cslib_dflt.image_format));
    
end

fclose(fID);

