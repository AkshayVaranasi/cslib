% Description: plots the local coherences of F*Phi, where F and Phi are the
% DFT and Haar DWT respectively
%
% Ben Adcock, Vegard Antun 2016

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% Create destination for the plots
if (exist('plots') ~= 7) 
    mkdir('plots');
end
dest = 'plots/';
disp_plots = 'off';
dwtmode('per', 'nodisp');

% Parameters
nu = 8; % 2^nu = signal size

% Dependent parameters
N = 2^(nu);

%% Compute F*Phi

% Compute Fourier matrix
F = dftmtx(N)/sqrt(N);
F = fftshift(F,1);

wave_name = {'Haar', 'db6'};
for t = 1:length(wave_name)

    % Compute Phi for wavelets
    Phi = zeros(N);
    I = eye(N);
    S = get_wavedec_s(nu, wmaxlev(N,'Haar')-1);
    for i=1:N
        Phi(:,i) = waverec(I(:,i), S, 'Haar');
    end
    FPhi = F*Phi;

    %% plot F*Phi with lines
    figure('Visible', disp_plots);
    img = imagesc(abs(FPhi)/max(max(abs(FPhi))));

    colormap('parula');
    axis('off');
    axis('equal');
    colorbar('Fontsize', cslib_dflt.font_size, ...
             'LineWidth',cslib_dflt.cbar_line_width);
    
    hold('on');

    lw = 1.5;

    for k=1:nu-1
        x = [2^k, 2^k];
        y = [1, N];
        plot(x,y,'r','LineWidth',lw);
    end

    for k=1:nu-1
        x = [1 N];
        y = (N/2 + 2^(k-1))*[1 1];
        plot(x,y,'r','LineWidth',lw);
        y = (N/2 - 2^(k-1))*[1 1];
        plot(x,y,'r','LineWidth',lw);
    end

    hold('off');
    saveas(img, sprintf('%sFourier%sMatrixLines2', dest, wave_name{t}), ...
           cslib_dflt.plot_format);

    %%

    X = abs(FPhi);
    Z = zeros(N);


    for k=0:nu-1
        
        if k~=0
            samp_level = [-2^k+1:-2^(k-1), 2^(k-1)+1:2^k];
        else
            samp_level = [0, 1];
        end
        
        samp_level = samp_level + ones(size(samp_level))*N/2;
        
        for l=1:nu
            
            if l~=1
            sparse_level = 2^(l-1)+1:2^l;
            else
                sparse_level = 1:2;
            end
            
            m = length(samp_level);
            n = length(sparse_level);
            mu = sqrt(m)*(max(max(X(samp_level,sparse_level))));
            Z(samp_level,sparse_level) = mu*ones(m,n);
            
        end
    end

    % clip Z above 1
    Z(Z>1)=1;

    figure('Visible', disp_plots);
    img = imagesc(Z);

    colormap('parula');
    axis('off');
    axis('equal');
    colorbar('Fontsize', cslib_dflt.font_size, ...
             'LineWidth',cslib_dflt.cbar_line_width);

    hold('on');
    lw = 1.5;

    for k=1:nu-1
    x = [2^k 2^k];
    y = [1 N];
    plot(x,y,'r','LineWidth',lw);
    end

    for k=1:nu-1
        x = [1 N];
        y = (N/2 + 2^(k-1))*[1 1];
        plot(x,y,'r','LineWidth',lw);
        y = (N/2 - 2^(k-1))*[1 1];
        plot(x,y,'r','LineWidth',lw);
    end
    hold('off');

    saveas(img, sprintf('%sFourier%sMatrixCoherences', dest, wave_name{t}), ...
           cslib_dflt.plot_format);

end
