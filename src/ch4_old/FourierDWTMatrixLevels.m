% Description: plots F*Phi, the sparsity levels and sampling levels where F
% and Phi are the DFT and Haar DWT respectively
%
% Ben Adcock, Vegard Antun 2016

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% Create destination for the plots
if (exist('plots') ~= 7) 
    mkdir('plots');
end
dest = 'plots/';
disp_plots = 'off';
dwtmode('per', 'nodisp');

% Parameters
nu = 8; % 2^nu = signal size

% Dependent parameters
N = 2^(nu);

%% Compute F*Phi

% Compute Fourier matrix
F = dftmtx(N)/sqrt(N);
F = fftshift(F,1);

% Compute Phi for Haar wavelets
Phi = zeros(N);
I = eye(N);
S = get_wavedec_s(nu, wmaxlev(N,'Haar')-1);
for i=1:N
    Phi(:,i) = waverec(I(:,i), S, 'Haar');
end
FPhi = F*Phi;

% plot F*Phi
figure('Visible', disp_plots);
img = imagesc(abs(FPhi)/max(max(abs(FPhi))));
colormap('parula');
axis('off');
axis('equal');
colorbar('Fontsize', cslib_dflt.font_size, ...
         'LineWidth',cslib_dflt.cbar_line_width);
saveas(img, strcat(dest,'FourierHaarMatrix'), cslib_dflt.plot_format);

%% Compute sampling levels
X = abs(FPhi);
Z = ones(N);

for k = 1:nu-1
    for i = 2^(k-1)+1:2^k
        Z(i+N/2,:) = k+1;
        Z(N/2+1-i,:) = k+1;
    end  
end

% Plot sampling levels
figure('Visible', disp_plots);
img = imagesc(Z);
axis('off');

cmap = colormap(parula(8));
cmap = flipud(cmap);

colormap(cmap);
cbar = colorbar('Fontsize',cslib_dflt.font_size,...
                'LineWidth',cslib_dflt.cbar_line_width);

cbar.Ticks = (1+nu/(2*nu+1)):nu/(nu+1):nu;
cbar.TickLabels = {'Level 1','Level 2','Level 3','Level 4','Level 5',...
                   'Level 6','Level 7','Level 8'};
cbar.TickLength = 0.0;

saveas(img,strcat(dest,'FourierHaarSamplingLevels'), cslib_dflt.plot_format);

%% Compute sparsity levels
Z = ones(N);

for k=1:nu
    for i=2^(k-1):2^k
       Z(:,i) = k; 
    end
end

% Plot sparsity levels
figure('Visible', disp_plots);
img = imagesc(Z);
axis('off');

cmap = colormap(parula(8));
cmap = flipud(cmap);

colormap(cmap);
cbar = colorbar('Fontsize',cslib_dflt.font_size, ...
                'LineWidth', cslib_dflt.cbar_line_width);

cbar.Ticks = (1+nu/(2*nu+1)):nu/(nu+1):nu;
cbar.TickLabels = {'Scale 1','Scale 2','Scale 3','Scale 4','Scale 5',...
                   'Scale 6','Scale 7','Scale 8'};
cbar.TickLength = 0.0;

saveas(img, strcat(dest,'FourierHaarSparsityLevels'), cslib_dflt.plot_format);

%% Add red lines to FPhi

% plot F*Phi
figure('Visible', disp_plots);
img = imagesc(abs(FPhi)/max(max(abs(FPhi))));
colormap('parula');
cbar = colorbar('Fontsize',cslib_dflt.font_size, ...
                'LineWidth', cslib_dflt.cbar_line_width);
axis('off')

hold('on');

lw = 1.5;

for k=1:nu-1  
    x = [2^k 2^k];
    y = [1 N];
    plot(x,y,'r','LineWidth',lw);
end

for k=1:nu-1
    x = [1 N];
    y = (N/2 + 2^(k-1))*[1 1]; 
    plot(x,y,'r','LineWidth',lw);
    y = (N/2 - 2^(k-1))*[1 1]; 
    plot(x,y,'r','LineWidth',lw);
end

hold('off');

saveas(img, strcat(dest, 'FourierHaarMatrixLines'), cslib_dflt.plot_format);

