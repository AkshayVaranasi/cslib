% Description: Plots a sparse vector and a sparse in levels vector
% s = 12, (s_1,s_2,s_3,s_4) = (3,2,4,3)
%
% Ben Adcock, Vegard Antun 2016

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% Create destination for the plots
if (exist('plots') ~= 7) 
    mkdir('plots');
end
dest = 'plots/';
disp_plots = 'off';
dwtmode('per', 'nodisp');

% Parameters
nu = 7; % 2^nu = signal size

% Dependent parameters
N = 2^(nu);



% sparse in levels
x1 = zeros(N,1);
supp = [10 ; 15 ; 25 ; 40 ; 56 ; 65 ; 81 ; 86 ; 92 ; 99 ; 108 ; 123 ];
x1(supp) = randn(size(supp));


% sparse
x2 = zeros(N,1);
supp = [12 ; 33 ; 35 ; 39 ; 42 ; 46 ; 48 ; 51 ; 55 ; 58 ; 62 ; 89  ];
x2(supp) = randn(size(supp));

%% plot x1 and x2
fig1 = figure('Visible', disp_plots);

ms = 8;
lw = 2;
stem(x1,'o', 'MarkerSize',cslib_dflt.marker_size, ...
             'MarkerFaceColor', cslib_dflt.blue, ...
             'LineWidth',cslib_dflt.line_width);

xlim([1 N+1]);
ymax = max(abs(x1))*1.1;
ylim([-ymax,ymax]);

hold('on');
plot(32*[1 1],[-ymax ymax],'r--','LineWidth', cslib_dflt.line_width);
plot(64*[1 1],[-ymax ymax],'r--','LineWidth', cslib_dflt.line_width);
plot(96*[1 1],[-ymax ymax],'r--','LineWidth', cslib_dflt.line_width);
hold('off')

ax = gca;
ax.YMinorTick = 'off';
ax.YMinorGrid = 'off';
ax.FontSize = cslib_dflt.font_size;
ax.LineWidth = cslib_dflt.line_width;
ax.YGrid = 'on';
ax.XGrid = 'on';
ax.XTick = [1, 32, 64, 96, 128];

saveas(fig1, strcat(dest,'sparse_vector'), cslib_dflt.plot_format);

fig2 = figure('Visible', disp_plots);

stem(x2, 'o', 'MarkerSize', cslib_dflt.marker_size, ...
              'MarkerFaceColor', cslib_dflt.blue, ...
              'LineWidth', cslib_dflt.line_width);
xlim([1, N+1]);
ymax = max(abs(x1))*1.1;
ylim([-ymax, ymax]);

hold('on');
plot(32*[1, 1],[-ymax, ymax],'r--','LineWidth', cslib_dflt.line_width);
plot(64*[1, 1],[-ymax, ymax],'r--','LineWidth', cslib_dflt.line_width);
plot(96*[1, 1],[-ymax, ymax],'r--','LineWidth', cslib_dflt.line_width);
hold('off');

ax = gca;
ax.YMinorTick = 'off';
ax.YMinorGrid = 'off';
ax.FontSize = cslib_dflt.font_size;
ax.LineWidth = cslib_dflt.line_width;
ax.YGrid = 'on';
ax.XGrid = 'on';
ax.XTick = [1, 32, 64, 96, 128];

saveas(fig2, strcat(dest, 'sparse_in_levels_vector'), ...
             cslib_dflt.plot_format);

