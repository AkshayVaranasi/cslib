%% construct vectors
%
% Ben Adcock, Vegard Antun 2016

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% Create destination for the plots
if (exist('plots') ~= 7) 
    mkdir('plots');
end
dest = 'plots/';
disp_plots = 'off';
dwtmode('per', 'nodisp');

% Parameters
nu = 4; % 2^nu = signal size

% Dependent parameters
N = 2^(nu);

n = N/nu;
s = 64; % total sparsity

%% Sparse and distributed
fig1 = figure('Visible', disp_plots);

x = zeros(N,1);
for k = 1:nu
    
    sl = s/nu;
    
    z = zeros(n,1);
    z(1:sl) = randn(sl,1);
    z = z(randperm(n));
    
    x((k-1)*n+1:k*n) = z;
    
end

stem(x,'o','MarkerSize', cslib_dflt.marker_size, ...
           'MarkerFaceColor', cslib_dflt.blue , ...
           'LineWidth', cslib_dflt.line_width, ...
           'Color', cslib_dflt.blue);
xlim([1 N+1]);
ymax = max(abs(x))*1.1;
ylim([-ymax,ymax]);

hold('on');
plot(64*[1 1],[-ymax ymax],'r--','LineWidth',  cslib_dflt.line_width);
plot(128*[1 1],[-ymax ymax],'r--','LineWidth', cslib_dflt.line_width);
plot(192*[1 1],[-ymax ymax],'r--','LineWidth', cslib_dflt.line_width);
hold('off');

ax = gca;
ax.YMinorTick = 'off';
ax.YMinorGrid = 'off';
ax.FontSize = cslib_dflt.font_size;
ax.LineWidth = cslib_dflt.line_width;
ax.YGrid = 'on';
ax.XGrid = 'on';
ax.XTick = [1, 64, 128, 192, 256];

saveas(fig1, strcat(dest, 'sparse_distributed_vector'), ...
       cslib_dflt.plot_format);

%% Sparse but not distributed
fig2 = figure('Visible', disp_plots);

x = zeros(N,1);

q = 12;

z = zeros(n+q,1);
z(1:s) = randn(s,1);
z = z(randperm(n+q));

x(n+1:2*n+q) = z;

stem(x, 'o', 'MarkerSize',cslib_dflt.marker_size, ... 
             'MarkerFaceColor',cslib_dflt.blue, ...
             'LineWidth',cslib_dflt.line_width,...
             'Color',cslib_dflt.blue);
xlim([1 N+1]);
ymax = max(abs(x))*1.1;
ylim([-ymax,ymax]);

hold('on');
plot( 64*[1, 1], [-ymax, ymax], 'r--', 'LineWidth', cslib_dflt.line_width);
plot(128*[1, 1], [-ymax, ymax], 'r--', 'LineWidth', cslib_dflt.line_width);
plot(192*[1, 1], [-ymax, ymax], 'r--', 'LineWidth', cslib_dflt.line_width);
hold('off');

ax = gca;
ax.YMinorTick = 'off';
ax.YMinorGrid = 'off';
ax.FontSize = cslib_dflt.font_size;
ax.LineWidth = cslib_dflt.line_width;
ax.YGrid = 'on';
ax.XGrid = 'on';
ax.XTick = [1, 64, 128, 192, 256];

saveas(fig2, strcat(dest,'sparse_clustered_vector'), ...
             cslib_dflt.plot_format);

%% compute lambda

S = [];
for k = 1:nu
   S = [S nnz(x((k-1)*n+1:k*n))];
end

lambda = max(S)*nu/s


