% Figure 3.3;
%

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

path_to_this_file = mfilename('fullpath');
path_to_this_file = path_to_this_file(1:end-length(mfilename)-1);
dest = fullfile(path_to_this_file, 'plots');

% Create destination for the plots
if (exist(dest) ~= 7) 
    mkdir(dest);
end

%DefineBrain; % Creates a struct called `Brain`.
%im = RasterizePhantom(Brain, N);

N = 2048;
sfactor = 8;
im_name = 'phantom_brain';
fname = sprintf('%s_%d.tiff', im_name, N);
X = double(imread(fullfile(cslib_dflt.data_path, 'MR', fname)));

Y = fftshift(fft2(X))/N;
keep  = N/sfactor;
Nh    = N/2;
keeph = keep/2;

idx = (Nh-keeph+1):(Nh+keeph);

Z = zeros([N,N]);
Z(idx,idx) = Y(idx,idx);

im = ifft2(ifftshift(Z))*N;
im = abs(im);

fname = sprintf('gibbs_%s_N_%d_sfact_%d.%s', im_name, N, sfactor, cslib_dflt.image_format);
imwrite(uint8(im), fullfile(dest,fname));

vs = round(N/16);
hs = round(N/2);
v = N/4; 

im_zoom =  im(vs:vs+v-1, hs:hs+v-1);
fname = sprintf('gibbs_%s_N_%d_sfact_%d_zoom.%s', im_name, N, sfactor, cslib_dflt.image_format);
imwrite(uint8(im_zoom), fullfile(dest,fname));

