% Figure 3.5
% Description: Perform a CS-experiment with various sampling fourier samplint
% patterns 
% - Gaussian distribution
% - 2 level
% - uniformly random
%
% Vegard Antun 2019
%

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

path_to_this_file = mfilename('fullpath');
path_to_this_file = path_to_this_file(1:end-length(mfilename)-1);
dest = fullfile(path_to_this_file, 'plots');

% Create destination for the plots
if (exist(dest) ~= 7) 
    mkdir(dest);
end

disp_plots = 'off';

vm = 4;                                     % Number of vanishing moments
curvelet_is_real = 0;                       % 0 => Complex valued curvelets
shearlet_levels = 4;                        % Number of shearlet levels
subsampling_rate = 0.05;                    % ∈ [0,1]
sigma = 1;                                  % min ||x||_1 s.t. ||Ax-b||_2 ≤ sigma
sigma_tv = 0.1;

dwtmode('per', 'nodisp');

N = 2048;
X=imread('../../data/phantom_brain.png');
X=double(rgb2gray(X));
xs = 780;
ys = 1470;
lx = 256;
ly = 256;
idx_crop_x = xs:xs+lx-1;
idx_crop_y = ys:ys+ly-1;

nbr_samples = round(subsampling_rate*N*N);

% Create sampling pattern
sparsities = csl_compute_sparsity_of_image(phantom(N), vm); 
%[idx, str_id] = spf2_DAS(N, nbr_samples, sparsities, vm);

fname_desc = fullfile(dest, 'plot_description.txt');
fID =fopen(fname_desc, 'a'); % Write a description of each image to a file

fully_sample = 0.020;
nbr_levels = 50;
p_norm = 2;

[idx, str_id] = spf2_exp(N, nbr_samples, fully_sample, nbr_levels, p_norm); 
length(idx)
% Fourier wavelet
fprintf('Computing wavelet reconstruction %s\n', str_id);
fname = sprintf('aFourier_wavelet_srate_%g_db%d_%s', subsampling_rate*100, vm, str_id);
[im_rec, wcoeff] = sample_fourier_wavelet(X, sigma, idx, fullfile(dest, fname), vm);
fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, sigma: %g\n', ...
                datestr(datetime('now')), fname, N, nbr_samples, sigma);

im_crop = im_rec(idx_crop_x, idx_crop_y);
filename = fullfile(dest, [fname, '_crop.', cslib_dflt.image_format]);
imwrite(im2uint8(im_crop), filename);

% Fourier Wavelet uniformly random
[idx, str_id] = sp2_uniform(N, nbr_samples);
fprintf('Computing wavelet reconstruction %s\n', str_id);
fname = sprintf('aFourier_srate_%g_db%d_wavlet_%s', subsampling_rate*100, vm, str_id);
[im_rec, wcoeff] = sample_fourier_wavelet(X, sigma, idx, fullfile(dest, fname), vm);
fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, sigma: %g\n', ...
                datestr(datetime('now')), fname, N, nbr_samples, sigma);

im_crop = im_rec(idx_crop_x, idx_crop_y);
filename = fullfile(dest, [fname, '_crop.', cslib_dflt.image_format]);
imwrite(im2uint8(im_crop), filename);


%% Fourier wavelet
p_norm = inf;
r_factor = 4;
[idx, str_id] = spf2_2level(N, nbr_samples, inf, r_factor);
fprintf('Computing wavelet reconstruction %s\n', str_id);
fname = sprintf('aFourier_srate_%g_db%d_wavlet_%s_%d', subsampling_rate*100, vm, str_id, r_factor);
[im_rec, wcoeff] = sample_fourier_wavelet(X, sigma, idx, fullfile(dest, fname), vm);
fprintf(fID, '%s: %s, N: %d, nbr_samples: %d, sigma: %g, patt: %s, r_factor: %d\n', ...
                datestr(datetime('now')), fname, N, nbr_samples, sigma, str_id, r_factor);

im_crop = im_rec(idx_crop_x, idx_crop_y);
filename = fullfile(dest, [fname, '_crop.', cslib_dflt.image_format]);
imwrite(im2uint8(im_crop), filename);

im_crop = im_rec(idx_crop_x, idx_crop_y);
filename = fullfile(dest, [fname, '_crop.', cslib_dflt.image_format]);
imwrite(im2uint8(im_crop), filename);

fclose(fID);


