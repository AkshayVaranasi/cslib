% Figure 3.7
% Reconstructs shepp logan phantom from 50 radial lines, using a radon sampling 
% operator.
%
clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% create destination for the plots
if (exist('plots') ~= 7) 
    mkdir('plots');
end
dest = 'plots/';
disp_plots = 'off';

dwtmode('per', 'nodisp');

% Set path to cslib_data
src = cslib_dflt.data_path;
N = 256;
views = 50;
vm = 2;      % Number of vanishing moments
fname_matrix = sprintf('radonMatrix2N%d_ang%d.mat', N, views);
fname_matrix = fullfile(src, 'radon_matrices', fname_matrix);
load(fname_matrix) % Matrix is named 'A'

wname = sprintf('db%d', vm);
nres = wmaxlev(N, wname)-1;

im = phantom(N);
sigma = 0.01;

noise = sigma*randn(size(A,1), 1);
b = A*im(:) 


radWaveOp = @(x, mode) dense2IDB2d(x, mode, A, wname, nres); 
% mode = 1  is the forward operator
% mode = 0 is the backward operator (transpose)

lambda = 1/sigma;


la_max = svds(A, 1)^2; % Compute singular value of A, and 
                       % square it, to get the larges eigenvalue 
                       % of A'*A. (Using that the wavelet transform 
                       % is a unitary matrix)  

L = 2*la_max;

lambda/L

tp = 1;

B = radWaveOp(b, 0); % transpose
xp = B; 
yp = xp;

% FISTA
for i = 1:1000
    i
    z = yp - (2/L)*( radWaveOp(radWaveOp(yp,1), 0) - B );
    x = sign(z).*max(abs(z) - (lambda/L), 0);
    t = (1 + sqrt(1+4*tp^2))/2;
    y = x + ((tp - 1)/t)*(x - xp);
    tp = t;
    yp = y;
    xp = x;
end



% Reconstruct image
s = get_wavedec2_s(round(log2(N)), nres);
im_rec_raw  = waverec2(x, s, wname);


im_rec = im_rec_raw;
idx = im_rec < 0;
im_rec(idx) = 0;
im_rec = (im_rec - min(im_rec(:)))/(max(im_rec(:)) - min(im_rec(:)));
im_rec = abs(im_rec);

%figure();
%subplot(121); imagesc(im_rec_raw); colormap('gray'); colorbar();
%subplot(122); imagesc(im_rec); colormap('gray'); colorbar();


fname_wave_raw  = fullfile(dest, sprintf('rec_radon_views_%d_N_%d_db%d_lasso_%g_raw.png', views, N, vm, lambda));
fname_wave_post = fullfile(dest, sprintf('rec_radon_views_%d_N_%d_db%d_lasso_%g_post.png', views, N, vm, lambda));

% Store the image
imwrite(im2uint8(scale_to_01(im_rec_raw)), fname_wave_raw);
imwrite(im2uint8(im_rec), fname_wave_post);


