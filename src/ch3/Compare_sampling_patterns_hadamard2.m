clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

path_to_this_file = mfilename('fullpath');
path_to_this_file = path_to_this_file(1:end-length(mfilename)-1);
dest = fullfile(path_to_this_file, 'plots');

% Create destination for the plots
if (exist(dest) ~= 7) 
    mkdir(dest);
end

disp_plots = 'off';

vm = 4;                                     % Number of vanishing moments
subsampling_rate = 0.04;                    % ∈ [0,1]
noise = 1;                                  % min ||x||_1 s.t. ||Ax-b||_2 ≤ noise

N = 512;

fname_core = 'brain1';
fname = sprintf('%s_%d.png', fname_core, N);
X = double(imread(fullfile(cslib_dflt.data_path, 'MR', fname)));
X = X/255;


nbr_samples = round(subsampling_rate*N*N);

% Create sampling pattern

[idx, str_id] = sp2_uniform(N, nbr_samples);
fprintf('Computing scrambled Hadamard wavelet db%d reconstruction with %s\n', vm, str_id);
fname = sprintf('aHadamard_scramble_wavelet_%s_srate_%d_db%d_%s', fname_core, 100*subsampling_rate, vm, str_id);
sample_scramble_hadamard_wavelet(X, noise, idx, fullfile(dest, fname), vm);

sparsities = csl_compute_sparsity_of_image(phantom(N), vm); 
[idx, str_id] = sph2_DAS(N, nbr_samples, sparsities);
% Hadamard wavelet
fprintf('Computing Hadamard wavelet db%d reconstruction with %s\n', vm, str_id);
fname = sprintf('aHadamard_wavelet_%s_srate_%d_db%d_%s',fname_core, 100*subsampling_rate, vm, str_id);
sample_hadamard_wavelet(X, noise, idx, fullfile(dest, fname), vm);

p_norm = 0.5;
full_sample = subsampling_rate;
nbr_levels = 30;
[idx, str_id] = sph2_exp(N, nbr_samples, full_sample, nbr_levels, p_norm);
% Hadamard wavelet
fprintf('Computing Hadamard wavelet db%d reconstruction with %s\n', vm, str_id);
fname = sprintf('aHadamard_wavelet_%s_srate_%d_db%d_%s',fname_core, 100*subsampling_rate, vm, str_id);
sample_hadamard_wavelet(X, noise, idx, fullfile(dest, fname), vm);

p_norm = 2;
full_sample = subsampling_rate/100;
nbr_levels = 30;
[idx, str_id] = sph2_exp(N, nbr_samples, full_sample, nbr_levels, p_norm);
% Hadamard wavelet
fprintf('Computing Hadamard wavelet db%d reconstruction with %s\n', vm, str_id);
fname = sprintf('aHadamard_wavelet_%s_srate_%d_db%d_%s', fname_core, 100*subsampling_rate, vm, str_id);
sample_hadamard_wavelet(X, noise, idx, fullfile(dest, fname), vm);

r_factor = 4;
[idx, str_id] = sph2_2level(N, nbr_samples, inf, r_factor);
% Hadamard wavelet
fprintf('Computing Hadamard wavelet db%d reconstruction with %s\n', vm, str_id);
fname = sprintf('aHadamard_wavelet_%s_srate_%d_db%d_%s', fname_core, 100*subsampling_rate, vm, str_id);
sample_hadamard_wavelet(X, noise, idx, fullfile(dest, fname), vm);

