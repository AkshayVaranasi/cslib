% Figure 3.4
% Show a comparison between a wavelet and TV reconstruction on
% a MR brain image.
%
% Vegard Antun 2019

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

path_to_this_file = mfilename('fullpath');
path_to_this_file = path_to_this_file(1:end-length(mfilename)-1);
dest = fullfile(path_to_this_file, 'plots');

% Create destination for the plots
if (exist(dest) ~= 7) 
    mkdir(dest);
end

disp_plots = 'off';

vm = 4;                                     % Number of vanishing moments


subsampling_rate = 0.15;                    % ∈ [0,1]
noise = 1e-2;                                % min ||x||_1 s.t. ||Ax-b||_2 ≤ noise
noise_tv = 1e-5;

N = 512;
%N = 836;
fname = sprintf('brain1_%d.png', N);
X = double(imread(fullfile(cslib_dflt.data_path, 'MR', fname)));

nbr_samples = round(subsampling_rate*N*N);

% Create sampling pattern
%sparsities = csl_compute_sparsity_of_image(phantom(N), vm); 
%[idx, str_id] = spf2_DAS(N, nbr_samples, sparsities, vm);

fully_sample = 0.03;
nbr_levels = 30;
p_norm = 2;

[idx, str_id] = spf2_exp(N, nbr_samples, fully_sample, nbr_levels, p_norm); 

wname = 'db4';
nres = 4;
sigma = 0.01;
noise_parameter = sqrt(nbr_samples)*sigma;
measurement_noise = sigma*randn(nbr_samples, 1);

l = 150;
lh= l/2;
v = 100;
h = 100;
idx_v = v:v+lh-1;
idx_h = h:h+l-1;
%figure();
%subplot(121); imagesc(X); colormap('gray');
%subplot(122); imagesc(X(idx_v, idx_h)); colormap('gray'); axis('equal');


% Fourier wavelet
fprintf('Computing wavelet reconstruction\n');
fname = sprintf('aFourier_wavelet_nesta_N_%d_srate_%g_db%d_%s',N, subsampling_rate*100, vm, str_id);
fname_wave_zoom =sprintf('aFourier_wavelet_nesta_N_%d_srate_%g_db%d_%s_zoom',N, subsampling_rate*100, vm, str_id); 
[im_rec1, z] = sample_fourier_wavelet_nesta(X, sqrt(nbr_samples)*sigma, idx, fullfile(dest, fname), vm, 'measurement_noise', measurement_noise);
imwrite(im_rec1(idx_v, idx_h), fullfile(dest,[fname_wave_zoom, '.', cslib_dflt.image_format]));

% Fourier TV
fprintf('Computing TV reconstruction\n');
fname = sprintf('aFourier_TV_N_%d_srate_%g_%s', N, subsampling_rate*100, str_id);
fname_tv_zoom = sprintf('aFourier_TV_N_%d_srate_%g_%s_zoom', N, subsampling_rate*100, str_id);
fprintf('Fourier TV reconstruction\n')
[im_rec2, z] = sample_fourier_TV(X, sqrt(nbr_samples)*sigma, idx, fullfile(dest, fname));

imwrite(im_rec2(idx_v, idx_h), fullfile(dest,[fname_tv_zoom, '.', cslib_dflt.image_format]));


















%% Fourier wavelet
%fprintf('Computing wavelet reconstruction\n');
%fname = sprintf('aFourier_wavelet_srate_%g_db%d_%s', subsampling_rate*100, vm, str_id);
%sample_fourier_wavelet(X, noise, idx, fullfile(dest, fname), vm);










