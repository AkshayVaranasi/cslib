function y=had2IDB2d_scaled(x, mode, N, idx, scales, nres, wave_name, ...
                                                           hadamard_order);

    if mode == 1 

        S = get_wavedec2_s(round(log2(N)), nres); 
        z = waverec2(x, S, wave_name);
        z = fastwht2(z, hadamard_order);
        y = scales(idx).*z(idx);

    else % Transpose

        z = zeros(N*N, 1);
        z(idx) = x;
        z = scales.*z;
        z = reshape(z, [N,N]);
        z = fastwht2(z, hadamard_order);
        [y, S] = wavedec2(z, nres, wave_name);
        y = y';

    end
end






