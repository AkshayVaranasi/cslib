% The function create a bernoulli sensing matrix. This matrix can potentially 
% require more memory than your computer can provide. If so, the image X
% will be resized so that the sensing matrix can be stored in memory 
% 
% INPUT
% X - Image
% M - Number of samples
% 
% OUTPUT
% A - Bernoulli Sensing mantrix of size M × numel(X)
% X - The image X given as input, but potentially a resized version of it.
% M - The current number of measuremets. If the image is resized, the number of 
%     samples will be reduced accordingly.
%
% Edvard Aksnes July 2017
%
function [ A, X, M ] = generateBernoulliMatrix( X, M)
    A=zeros(1,1);
    N=numel(X);
    tryGenA=true;
    while (tryGenA)
        try % see if the computer can support this problem size
            A = sign(randn(M,N))/sqrt(M);
            tryGenA=false;
        catch err % if not, then reduce the problem size
          % err
          if (strcmp(err.identifier,'MATLAB:nomem') ||  ...
              strcmp(err.identifier,'MATLAB:array:SizeLimitExceeded'))
              M=round(M/2);
              X=imresize(X,0.5);
              N=numel(X);
              display('Reduced problem size since measurement matrix could not fit into memory!')
              % continue;
          end
        end
    end

end
