% This file run the flip test 

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% Create destination for the plots
if (exist('plots') ~= 7) 
    mkdir('plots');
end
dest = 'plots/';
disp_plots = 'off';

vm = 4;                                     % Number of vanishing moments
subsampling_rate = 0.20;                    % ∈ [0,1]
sigma = 50;                                  % min ||x||_1 s.t. ||Ax-b||_2 ≤ sigma

% Read image, and reduce its size such that we are able to handle the Bernoulli 
% matrix in memory.
im = imread('../../data/peppers.tiff');
im = double(rgb2gray(im));
%X = im2double(imresize(X,0.5));

N = size(im, 1);
M = round(subsampling_rate*N*N);


% Create sampling pattern
idx = sph2_rect(N, M);

filename = sprintf('%speppers_rec_vm%d_sub%d_sig%d', dest, vm, ...
                   round(100*subsampling_rate), sigma);


flip_test(im, sigma, idx, filename, vm);



