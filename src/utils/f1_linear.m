%  Test this function, I belive there is a bug there somewhere

% This function order the fourier coefficients according to frequency. Hence
% the order becomes {0, -1, 1, -2, 2, -3, ... }. It is assumed that the
% original order of `U` is {0,1,2, ... ,-3, -2, -1}.  
function Y = f1_linear(U)
    N = size(U,1);
    Y = zeros(size(U));
    Y(1:2:N,:) = U(1:N/2,:);
    Y(2:2:N,:) = U(N:-1:N/2+1,:);
end


