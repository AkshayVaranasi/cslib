


function Y = f1_linear_inv(U)
    N = size(U,1);
    Y = zeros(size(U));
    Y(1:N/2, :)    = U(1:2:N,:);
    Y(N:-1:N/2+1,:) = U(2:2:N,:);
end




