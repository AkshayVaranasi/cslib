% sample_hadamard_wavelet() performs a compressive sensing experiment with a
% Hadamard sampling operator and a Daubechies wavelet as the sparsifying
% operator. The image `im` is sampled in the Walsh-Hadamard domain, at the
% indices specified in `idx`. The samples are then stored in a vector, say b,
% and we solve the optimization problem 
%              minimize ||x||_1  s.t.  ||Ax - b||_2 <= sigma
% using the SPGL1 package. Here A is the matrix obtained by right multiplying
% the Hadamard operator with an inverse wavelet operator. Finally both the 
% reconstructed image and it's sampling map is stored as images. 
% 
% The function stores the reconstructed image and its sampling pattern as 
% 'filename' and 'filename_samp' with the file extension specified by
% cslib_dflt.image_format (see etc/set_defaults.m)
%
% INPUT
% im       - Image which will be sampled
% sigma    - Error threshold 
% idx      - The matrix indices one would like to sample, given in an linear
%            order (see sub2ind(..) for conversion to linear order)
% scales   - Array with elements which will be used to scale the change of
%            basis matrix
% filename - Name of file without the format extension.
% vm       - Number of vanishing moments of the Daubechies wavelet
%
% OUTPUT
% im_rec - The recovered image
%  

% OBS: This fuction should return im_rec, but until the end of may it will
% return z instead. 
function [im_rec, z] = sample_hadamard_wavelet_scaled(im, sigma, idx, scales, filename, vm);
    load('cslib_defaults.mat') % load font size, line width, etc.
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Verify that the input is correct and initialize all relevant variables %%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    csl_validate_sample_args(im, sigma, idx)     

    N = size(im, 1);
    M = length(idx);  % The number of samples

    % Store current boundary extension mode
    boundary_extension = dwtmode('status', 'nodisp');
    % Set the correct boundary extension for the wavelets
    dwtmode('per', 'nodisp');

    % Initialization
    wave_name = sprintf('db%d', vm);
    nres = wmaxlev(N, wave_name);          % Maximum wavelet decomposition level



    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%                Start the compressive sensing process                 %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    hadamard_order = 'sequency';    
     
    % Sample the image
    b = fastwht2(im);
    b = scales(idx).*b(idx);                            % Extract the sampled indices
    %b = b(idx);                            % Extract the sampled indices
     
    % Initialize the `A` matrix operator
    opA = @(x, mode) had2IDB2d_scaled(x, mode, N, idx, scales, nres, ...
                                      wave_name, hadamard_order);

    %  minimize ||x||_1  s.t.  ||Ax - b||_2 <= sigma
    opts = spgSetParms('verbosity', cslib_dflt.spgl1_verbose);
    z    = spg_bpdn(opA, b, sigma, opts); 
    %z = z./scales;
    % Reconstruct image 
    s = get_wavedec2_s(round(log2(N)), nres);
    im_rec  = waverec2(z, s, wave_name);

    im_rec = (im_rec - min(im_rec(:)))/(max(im_rec(:)) - min(im_rec(:)));

    % Store the image
    imwrite(im2uint8(im_rec), sprintf('%s.%s', filename, ...
                                               cslib_dflt.image_format));

    % Store the sampling map
    Y = zeros([N, N] ,'uint8');
    Y(idx) = 255;
    imwrite(im2uint8(Y), sprintf('%s_samp.%s', filename, ...
                                               cslib_dflt.image_format));

    % Restore dwtmode
    dwtmode(boundary_extension, 'nodisp')
    
end





