function [im_rec, z] = sample_hadamard_wavelet(im, sigma, idx, filename, vm, varargin);
    load('cslib_defaults.mat') % load font size, line width, etc.
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Verify that the input is correct and initialize all relevant variables %%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    csl_validate_sample_args(im, sigma, idx)     
    
    N = size(im, 1);
    M = length(idx);                       % The number of samples
    
    % Initialization
    wave_name = sprintf('db%d', vm);
    opts.wave_levels = wmaxlev(N, wave_name);  % Maximum wavelet decomposition level
    opts = csl_argparse(opts, varargin); 
     
    % Store current boundary extension mode
    boundary_extension = dwtmode('status', 'nodisp');
    % Set the correct boundary extension for the wavelets
    dwtmode('per', 'nodisp');
     
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%                Start the compressive sensing process                 %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    hadamard_order = 'sequency';    
    perm = randperm(N*N);
    a = find(perm == 1);
    tmp_elem = perm(1);
    perm(1) = 1;
    perm(a) = tmp_elem;
    perm_rev(perm) = 1:N*N;
    
    im_perm = im(perm);
    % Sample the image
    b = fastwht2(reshape(im_perm, [N,N]));
    b = b(idx);                            % Extract the sampled indices

    % Initialize the `A` matrix operator
    opA = @(x, mode) had_scramble2IDB2d(x, mode, N, idx, opts.wave_levels,...
                                        wave_name, perm, perm_rev, hadamard_order);

    %  minimize ||x||_1  s.t.  ||Ax - b||_2 <= sigma
    opts_spgl1 = spgSetParms('verbosity', cslib_dflt.spgl1_verbose);
    z    = spg_bpdn(opA, b, sigma, opts_spgl1); 

    % Reconstruct image 
    s = get_wavedec2_s(round(log2(N)), opts.wave_levels);
    im_rec  = waverec2(z, s, wave_name);

    im_rec = (im_rec - min(im_rec(:)))/(max(im_rec(:)) - min(im_rec(:)));

    % Store the image
    imwrite(im2uint8(im_rec), sprintf('%s.%s', filename, ...
                                               cslib_dflt.image_format));

    % Store the sampling map
    Y = zeros([N, N] ,'uint8');
    Y(idx) = 255;
    imwrite(im2uint8(Y), sprintf('%s_samp.%s', filename, ...
                                               cslib_dflt.image_format));

    % Restore dwtmode
    dwtmode(boundary_extension, 'nodisp')

end

