clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.
dwtmode('per', 'nodisp');

% create destination for the plots
dest = 'plots/';
dest_data = 'psnr_data';
if (exist(dest) ~= 7) 
    mkdir(dest);
end
if (exist(dest_data) ~= 7) 
    mkdir(dest_data);
end

r_id = read_counter('./COUNT.txt');
fname = sprintf('run%03d', r_id);
dest_data_full = fullfile(dest_data, fname);
 
if (exist(dest_data_full) ~= 7) 
    mkdir(dest_data_full);
end
fprintf('--------------- runner id: %d ----------------\n', r_id);

% Load config
%config_psnr;
config_name = fullfile(dest_data_full, 'config_psnr.m');
copyfile('config_psnr.m', config_name);
run(config_name);

wname = sprintf('db%d', vm);
image_name = fullfile( im_dir, sprintf('%s_%d.png', im_name_core, N) );
im_uint8 = imread(fullfile(cslib_dflt.data_path, image_name));
im = double(im_uint8);

nbr_of_patterns = length( samp_patt_handles );
nbr_of_srates = length( subsampling_rates );

for i_patt = 1:nbr_of_patterns;
    f = samp_patt_handles{i_patt};
    psnr_arr = zeros([nbr_of_srates, 1]);
    snr_arr  = zeros([nbr_of_srates, 1]);
    for j = 1:nbr_of_srates    

        nbr_samples = round(N*N*subsampling_rates(j));
        [idx, str_id] = f(N, nbr_samples);
        Patt = zeros([N,N]);
        Patt(idx) = 1;

        if sum(Patt(:)) ~= nbr_samples
            fprintf('Warning: patt: %s, did only produce %d samples, requested %d\n', ...
                    str_id, sum(Patt(:)), nbr_samples);
        end

        fname = fullfile(dest, 'not_relevant');

        progressbar(j, nbr_of_srates);

        [im_rec, z] = sample_fourier_wavelet(im, noise_level, idx, fname, ...
                                             vm, 'spgl1_verbose', 0);

        nres   = wmaxlev(N, wname);  % Maximum wavelet decomposition level
        S      = get_wavedec2_s(round(log2(N)), nres);
        im_rec = waverec2(z, S, wname);

        im_rec = abs(im_rec);
        idx_rem = im_rec > 255;
        im_rec(idx_rem) = 255;

        [psnr_val, snr_val] = psnr(im_rec, im, 255);

        psnr_arr(j) = psnr_val;
        snr_arr(j)  = snr_val;

    end

    fname_data_psnr = sprintf('psnr_%d.mat', i_patt);
    fname_data_snr  = sprintf('snr_%d.mat', i_patt);

    save(fullfile(dest_data_full, fname_data_psnr), 'psnr_arr');
    save(fullfile(dest_data_full, fname_data_snr),  'snr_arr');

end




