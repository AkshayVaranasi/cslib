
dwtmode('per', 'nodisp');

vm = 4;
r = 10; 
N = 2^r;

im_dir = 'natural_images';
im_name_core = 'kopp3';
noise_level = 0.1;
nres = wmaxlev(N, sprintf('db%d', vm));

subsampling_rates = linspace(0.05, 0.5, 25);


vm = 4;
wname = sprintf('db%d', vm);
nres = wmaxlev(N, wname);

a = 2;
r0 = 2;
nbr_levels = 50;
f1 = @(N, nbr_samples) sph2_gcircle(N, nbr_samples, a, r0, nbr_levels);
str_id1 = sprintf('exp, l_%d, a: %d, r0: %d, r:%d', 2, a, r0, nbr_levels);

a = 1;
r0 = 2;
nbr_levels = 50;
f2 = @(N, nbr_samples) sph2_gcircle(N, nbr_samples, a, r0, nbr_levels);
str_id2 = sprintf('exp, l_%d, a: %d, r0: %d, r:%d', 2, a, r0, nbr_levels);


a = 2;
r0 = 2;
nbr_levels = 50;
f3 = @(N, nbr_samples) sph2_gsquare(N, nbr_samples, a, r0, nbr_levels);
str_id3 = sprintf('exp, l_inf, a: %d, r0: %d, r:%d', a, r0, nbr_levels);

a = 1;
r0 = 2;
nbr_levels = 50;
f4 = @(N, nbr_samples) sph2_gsquare(N, nbr_samples, a, r0, nbr_levels);
str_id4 = sprintf('exp, l_inf, a: %d, r0: %d, r:%d', a, r0, nbr_levels);

p_norm = inf;
r_factor = 4;
f5 = @(N, nbr_samples) sph2_2level(N, nbr_samples, p_norm, r_factor);
str_id5 = sprintf('2 level, l_inf, m/%d', r_factor);

im = phantom(N);
sparsity = csl_compute_sparsity_of_image(im, vm);
f6 = @(N, nbr_samples) sph2_DAS(N, nbr_samples, sparsity);
str_id6 = 'DAS';

sValues    = csl_calculate_sparsities(im, nres, vm);
f7 = @(N, nbr_samples) sph2_DAS_randy(N, nbr_samples, sValues);
str_id7 = 'DAS_Randy';

f8 = @(N, nbr_samples) sph2_DIS(N, nbr_samples, sValues);
str_id8 = 'DIS';


a = 1;
r0 = 2;
nbr_levels = 50;
radius = 2;
p_norm = 0.5;
f9 = @(N, nbr_samples) sph2_exp(N, nbr_samples, a, r0, nbr_levels, radius, p_norm);
str_id9 = sprintf('exp, l_%g, a: %d, r0: %d, r:%d, rad: %G',p_norm, a, r0, nbr_levels, radius);

a = 2;
r0 = 2;
nbr_levels = 50;
radius = 2;
p_norm = 0.5;
f10 = @(N, nbr_samples) sph2_exp(N, nbr_samples, a, r0, nbr_levels, radius, p_norm);
str_id10 = sprintf('exp, l_%g, a: %d, r0: %d, r:%d, rad: %G',p_norm, a, r0, nbr_levels, radius);


samp_patt_handles = {f1, f2, f3, f4, f5, f6, f7, f8, f9, f10};
str_identifier = {str_id1, str_id2, str_id3, str_id4, str_id5, ...
                  str_id6, str_id7, str_id8, str_id9, str_id10};




