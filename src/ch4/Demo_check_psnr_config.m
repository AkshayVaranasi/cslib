clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

config_psnr;

image_name = fullfile( im_dir, sprintf('%s_%d.png', im_name_core, N) );
im_uint8 = imread(fullfile(cslib_dflt.data_path, image_name));
im = double(im_uint8);

nbr_patterns = length(samp_patt_handles);

for i = 1:nbr_patterns
    f = samp_patt_handles{i};
    str_id = str_identifier{i}
    for srate = subsampling_rates
        nbr_samples = round(srate*N*N);
        [idx, str_id] = f(N, nbr_samples);
    end
end

