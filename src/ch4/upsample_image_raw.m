function X = upsample_image_raw(im, factor);

    if length(size(im))> 2
        error('Can only handle 2D images');
    end

    [M,N] = size(im);
   
    X = zeros([factor*M, factor*N]);
    
    for i = 1:factor
        idx_x = i:factor:factor*M;
        for j = 1:factor
            idx_y = j:factor:factor*N;
            X(idx_x, idx_y) = im;
        end
    end
end
