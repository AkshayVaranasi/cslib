clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.


r_id = 36;
disp_plots = 'off';
dest = 'plots';

data_full_path = fullfile('psnr_data', sprintf('run%03d', r_id));
run(fullfile(data_full_path, 'config_psnr.m'));

nbr_patterns = length(str_identifier);
psnr_curves= cell([nbr_patterns, 1]);

for i = 1:nbr_patterns

    fname = sprintf('psnr_%d.mat', i);
    load(fullfile(data_full_path, fname)); 
    psnr_curves{i} = psnr_arr;
end

fig = figure('visible', disp_plots);
cmap = jet(nbr_patterns);

for i = 1:nbr_patterns;
    plot(subsampling_rates, psnr_curves{i}, 'linewidth', cslib_dflt.line_width, ...
         'Color', cmap(i,:));
    hold('on')
end
legend(str_identifier, 'Interpreter', 'none','location', 'northwest' );
title(sprintf('%s, N: %d', im_name_core, N), 'Interpreter', 'none');

fname = sprintf('psnr_curve_%s_run_%02d_N_%d.png', im_name_core, r_id, N);
saveas(fig, fullfile(dest, fname));


