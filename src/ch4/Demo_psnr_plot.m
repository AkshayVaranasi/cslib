clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

r_id = 8;
disp_plots = 'off';
dest = 'plots';
patt_names = {'2level', 'DIS', 'f_exp2'};
psnr_curves= cell(size(patt_names));

data_full_path = fullfile('psnr_data', sprintf('run%03d', r_id));
run(fullfile(data_full_path, 'config_psnr.m'));

for i = 1:length(patt_names)

    pname = patt_names{i};
    fname = sprintf('psnr_%s.mat', pname);
    load(fullfile(data_full_path, fname)); 
    psnr_curves{i} = psnr_arr;

end

fig = figure('visible', disp_plots);
for i = 1:length(patt_names);
plot(subsampling_rates, psnr_curves{i}, 'linewidth', cslib_dflt.line_width);
hold('on')
end
legend(patt_names, 'Interpreter', 'none','location', 'northwest' );
title(sprintf('%s, N: %d', im_name_core, N), 'Interpreter', 'none');

fname = sprintf('psnr_curve_%s_run_%d_N_%d.png', im_name_core, r_id, N);
saveas(fig, fullfile(dest, fname));


