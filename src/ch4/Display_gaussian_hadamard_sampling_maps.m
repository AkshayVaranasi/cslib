clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% create destination for the plots
if (exist('plots') ~= 7) 
    mkdir('plots');
end

dest = 'plots/';
disp_plots = 'off';

r = 8;
N = 2^r;
vm = 2;
nres = r-3;

nbr_levels = 50;
r0 = 2;

im = phantom(N);
sparsity = csl_compute_sparsity_of_image(im, vm);
sValues    = csl_calculate_sparsities(im, nres, vm);

for srate = [0.2]

    nbr_samples = round(srate*N*N);

    a = 1;
    [idx, str_id] = sph2_gcircle(N, nbr_samples, a, r0, nbr_levels);
    Z = zeros([N,N], 'uint8');
    Z(idx) = uint8(255);
    fname = sprintf('spatt_had_exp_l_2_r_%d_r0_%d_a_%d_srate_%d.%s', ...
    nbr_levels, r0, a, round(100*srate), cslib_dflt.image_format);
    imwrite(Z, fullfile(dest,fname));

    a = 2;
    [idx, str_id] = sph2_gcircle(N, nbr_samples, a, r0, nbr_levels);
    Z = zeros([N,N], 'uint8');
    Z(idx) = uint8(255);
    fname = sprintf('spatt_had_exp_l_2_r_%d_r0_%d_a_%d_srate_%d.%s', ...
    nbr_levels, r0, a, round(100*srate), cslib_dflt.image_format);
    imwrite(Z, fullfile(dest,fname));

    p_norm = 0.5;
    radius = 2;
    a = 1;
    [idx, str_id] = sph2_exp(N, nbr_samples, a, r0, nbr_levels, radius, p_norm);
    Z = zeros([N,N], 'uint8');
    Z(idx) = uint8(255);
    fname = sprintf('spatt_had_exp_l_05_r_%d_r0_%d_a_%d_srate_%d.%s', ...
    nbr_levels, r0, a, round(100*srate), cslib_dflt.image_format);
    imwrite(Z, fullfile(dest,fname));

    a = 2;
    [idx, str_id] = sph2_exp(N, nbr_samples, a, r0, nbr_levels, radius, p_norm);
    Z = zeros([N,N], 'uint8');
    Z(idx) = uint8(255);
    fname = sprintf('spatt_had_exp_l_05_r_%d_r0_%d_a_%d_srate_%d.%s', ...
    nbr_levels, r0, a, round(100*srate), cslib_dflt.image_format);
    imwrite(Z, fullfile(dest,fname));

    [idx, str_id] = sph2_DAS(N, nbr_samples, sparsity);
    Z = zeros([N,N], 'uint8');
    Z(idx) = uint8(255);
    fname = sprintf('spatt_had_DAS_srate_%d.%s', ...
                    round(100*srate), cslib_dflt.image_format);
    imwrite(Z, fullfile(dest,fname));

    [idx, str_id] = sph2_DIS(N, nbr_samples, sValues);
    Z = zeros([N,N], 'uint8');
    Z(idx) = uint8(255);
    fname = sprintf('spatt_had_DIS_srate_%d.%s', ...
                    round(100*srate), cslib_dflt.image_format);
    imwrite(Z, fullfile(dest,fname));

end
