# This file generates the file main.m. The file main.m is supposed to run all
# scripts in all the chX directories.
#
# Vegard Antun 2017
#


import os;
import sys;

fId = open('runAllScripts.m', 'w');

fId.write('% This file will run all scripts in all chX directories\n');
fId.write('%\n% Vegard Antun 2017\n%\n\n');

for dir_nr in range(1,5):
    dir_name = 'ch' + str(dir_nr);
    if (os.path.isdir(dir_name)):
        filenames = os.listdir(dir_name);
        for filename in filenames:
            if (filename[0].isupper() and filename[-2:] == '.m'):
                fId.write(filename[0:-2] + '\n');

fId.close();


