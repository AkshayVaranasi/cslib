% This script computes and stores an array of PSNR values for different values
% of lambda, when solving LASSO with FISTA.

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% create destination for the plots
dest = 'plots';
if (exist(dest) ~= 7) 
    mkdir(dest);
end
dest_data = 'data';
if (exist(dest_data) ~= 7) 
    mkdir(dest_data);
end

dwtmode('per', 'nodisp');

% Set path to cslib_data
src = cslib_dflt.data_path;
N = 256;
vm = 4;      % Number of vanishing moments
nbr_of_itr = 1000;
subsampling_rate = 0.25;
nbr_samples = round(N*N*subsampling_rate);

wname = sprintf('db%d', vm);
nres = wmaxlev(N, wname)-1;

im = phantom(N);

nbr_levels = 50;
r0 = 2;
a  = 2;

idx = spf2_gcircle(N, nbr_samples, a, r0, nbr_levels);

sigma = 1;

noise = sigma*randn([length(idx), 1]);
b = fourier2op(im(:), 1, N, idx);

measurements = b + noise;

L = 2;
lambda = 0.001/sigma;

opA = @(x, mode) fourier2IDB2d(x, mode, N, idx, nres, wname);

n = 40;
scaling_factor = linspace(-4,2,n);
pnsr_arr = zeros([n,1]);


for i = 1:n

    sf = scaling_factor(i);
    a = 10^(sf);
    lambda = a/sigma;

    sol = FISTA_solver(measurements, opA, nbr_of_itr, L, lambda);
    
    
    S = get_wavedec2_s(round(log2(N)), nres);
    im_rec  = waverec2(sol, S, wname);
    im_rec = abs(im_rec);
    idx_high = im_rec > 1;
    im_rec(idx_high) = 1;

    psnr_val = psnr(im_rec, im, 1);
    psnr_arr(i) = psnr_val;
    
end

fname = fullfile(dest_data, sprintf('psnr_fista_fourier_sigma_%g_N_%d.mat', sigma, N));
save(fname, 'psnr_arr', 'idx', 'nbr_of_itr', 'sigma', 'scaling_factor', 'L');



