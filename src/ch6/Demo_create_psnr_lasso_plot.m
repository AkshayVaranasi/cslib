% This script read the array produced by `Compute_psnr_lasso_lambda.m`
% and creates a plot of it.

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.
dwtmode('per', 'nodisp');

% create destination for the plots
dest = 'plots';
if (exist(dest) ~= 7) 
    mkdir(dest);
end
dest_data = 'data';
if (exist(dest_data) ~= 7) 
    mkdir(dest_data);
end
N = 2^8;
disp_plot = 'off';
sigma = 1;

fname = fullfile(dest_data, sprintf('psnr_fista_fourier_sigma_%g_N_%d.mat', sigma, N));
load(fname);


fig = figure('visible', disp_plot);
plot(scaling_factor, psnr_arr, 'linewidth', cslib_dflt.line_width);
xlabel('k, lambda = 10^{k}/sigma');
ylabel('PSNR')
title1 = sprintf('Shepp-Logan, N %d, max_itr: %d, sigma: %g', ...
                 N, nbr_of_itr,sigma);
title(title1);
fname = sprintf('Lasso_PSNR_sigma_%g_N_%d.png', sigma, N);
saveas(fig, fullfile(dest, fname));



