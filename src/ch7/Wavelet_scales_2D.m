% Applies a 2d wavelet transform to an image and writes the result to file.

clear('all') ; close('all');
load('cslib_defaults.mat') % load font size, line width, etc.

% Create destination for the plots
if (exist('plots') ~= 7) 
    mkdir('plots');
end
dest = 'plots/';
disp_plots = 'off';
dwtmode('per', 'nodisp');

N = 512;

im_name = 'phantom_brain.png';
im = im2double(imread(im_name));
im = rgb2gray(im);

im = imresize(im, [N,N]);

vm = 2;
wname = sprintf('db%d', vm);

[C, S] = wavedec2(im, 3, wname);
wave_coeff = abs(csl_wave_ord_2d_image(C, S, wname));
wave_coeff = (wave_coeff - min(wave_coeff(:)))/(max(wave_coeff(:)) - min(wave_coeff(:)));

out_im = im2uint8(wave_coeff.^(1/3));
fname = sprintf('wavelet_scales_2d_db%d.%s', vm, cslib_dflt.image_format);
imwrite(out_im, fullfile(dest, fname));


