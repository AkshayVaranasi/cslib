% spf2_uniform samples M samples uniformly at random from an N × N image
% 
% INPUT
% N - Image size i.e. N × N
% M - Number of samples
%
% OUTPUT:
% idx   - Sampling pattern, given in an linear ordering. That is indices in the range
%         1,2, ..., N*N.
% str_id - String identifyer, describing which type of sampling pattern this is.  
%
function [idx, str_id] = sp2_uniform(N, M)
    str_id = 'uniform';
    Y = zeros(N, N, 'uint8');
    pos = zeros(M, 2);
    s = 1;
    while (s <= M)
        i = round(1 + (N-1)*rand(1));
        j = round(1 + (N-1)*rand(1));
        if (~Y(i,j))
            pos(s,:) = [i, j];
            s = s + 1;
            Y(i,j) = 1;
        end
    end
    idx = sub2ind([N, N], pos(:,1), pos(:,2));
end

