% Creates a 2d Gaussian sampling pattern
%
% INPUT
% N - NxN image
% nbr_samples - The total number of samples
%
% OUTPUT:
% idx   - Sampling pattern, given in an linear ordering. That is indices in the range
%         1,2, ..., N*N.
% str_id - String identifyer, describing which type of sampling pattern this is.  
% 
% Edvard Aksnes, July 2017
%
function [idx, str_id] =spf2_gauss(N,nbr_samples)
    str_id = 'gauss';
    Y = zeros(N, N, 'uint8');
    pos = zeros(nbr_samples, 2);
    s = 1;
    while (s <= nbr_samples)
        expectation = [N*0.5,N*0.5]; % i.e centered in the middle of the image
        std_dev = [N*0.1,N*0.1];
        i = round(abs(normrnd(expectation(1),std_dev(1))));
        j = round(abs(normrnd(expectation(2),std_dev(2))));
        if ((i <= N) && (j <= N) && (0 <= j) && (0 <= i)) & (~Y(i,j)) 
            pos(s,:) = [i, j];
            s = s + 1;
            Y(i,j) = 1;
        end
    end
    idx = sub2ind([N, N], pos(:,1), pos(:,2));
end
