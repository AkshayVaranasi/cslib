% Computes the number of samples in each level
function nbr_samples_in_levels = csl_compute_nbr_samples_in_each_level(nbr_samples, density, bv, bh)
    
    if (isrow(bv) & ~iscolumn(bv))
        bv = bv';
    elseif (~iscolumn(bv))
        error('Vertical bounds must be vector');
    end
    
    if (isrow(bh) & ~iscolumn(bh))
        bh = bh';
    elseif( ~iscolumn(bh))
        error('Horizontal bounds must be vector');
    end
    
    rv = length(bv);
    rh = length(bh);

    a = bv(1:end)- [0; bv(1:end-1)];
    b = bh(1:end)- [0; bh(1:end-1)];
    max_level_size = a*b';
    
    g = @(c) func_for_bisection_method(c, density, max_level_size, nbr_samples);
    
    % Apply the bisection method to find the optimal constant for scaling.
    a = nbr_samples/2;
    b = nbr_samples*2;
    
    m = (a+b)/2;
    %g = @(c) f(c, density, max_level_size, nbr_samples);
    i = 0;
    while (g(a) > 0)
        a = a/2;
    end
    while(g(b)< 0)
        b = 2*b;
    end
    
    abserr = (b-a)/2;
    max_itr = round(log2(b-a)+8);
    
    while (i < max_itr & abserr > 1e-12*abs(m))
    
        if (g(m) == 0)
            a = m;
            b = m;
        end
        if (g(a)*g(m) < 0)
            b = m;
        else
            a = m;
        end
        i = i + 1;
        m = (a+b)/2;
        abserr = (b-a)/2;
    
    end
    
    nbr_samples_in_levels = round(m*density);
    leagal_levels = nbr_samples_in_levels <= max_level_size;
    nbr_samples_in_levels(~leagal_levels) = max_level_size(~leagal_levels);



    curr_nbr_of_samples = sum(nbr_samples_in_levels(:));


    if (curr_nbr_of_samples > nbr_samples) % Have to many samples 
    
        nbr_of_extra_samples = curr_nbr_of_samples - nbr_samples;
        [sorted, idx] = sort(leagal_levels(:), 'descend');
        idx1 = idx(1:nbr_of_extra_samples);
        nbr_samples_in_levels(idx1) = nbr_samples_in_levels(idx1) - 1; 
    
    end
    
    if (curr_nbr_of_samples < nbr_samples) % Have too few samples
        
        nbr_of_missing_samples = nbr_samples - curr_nbr_of_samples;
        [sorted, idx] = sort(leagal_levels(:), 'descend');
        idx1 = idx(1:nbr_of_missing_samples);
        nbr_samples_in_levels(idx1) = nbr_samples_in_levels(idx1) + 1; 
    
    end

end

% Computes the number of samples in each level, and subtracts the 
% desired number of samples i.e. it is only zero for the perfect constant c.
function s = func_for_bisection_method(c, density, max_level_size, nbr_samples) 

    nbr_samples_in_levels = round(c*density);
    leagal_levels = nbr_samples_in_levels <= max_level_size;
    nbr_samples_in_levels(~leagal_levels) = max_level_size(~leagal_levels);
    s = sum(nbr_samples_in_levels(:)) - nbr_samples;

end

