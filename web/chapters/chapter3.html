<div id="chapterMenu"></div>
<h1>Chapter 3</h1>

<h2>Introduction</h2>
<p>Traditional signal processing usually follows Shannon and Nyquist's sampling theorem. It states that a signal can be recovered perfectly if the sampling frequency is at least twice the highest frequency present in the signal.  However, this theorem assumes that the samples are taken with a uniform time interval, and that the reconstruction happens by interpolating the samples with sinc functions. </p>

<p>Compressive sensing is a new scheme for sampling and reconstructing which abandons these assumptions. It turns out that by placing different assumptions on the signal, and by using a different recovery strategy, we can recover our signal with less samples than the traditional theory suggests. </p>

<p>The field of compressive sensing has exploded in the last decade, after the initial publications by Candes, Tao, Romberg and Donoho. Some of the ideas and concepts have roots further back in time, but these publications mark the beginning of compressive sensing as a field of study. The key idea is to assume that the signal is \textit{sparse}, and then look for the sparsest possible signal which matches the sampled signal. A lot of new theory has been developed for recovering such signals. </p>

<p>During the development of this theory, one has found several applications of compressive sensing techniques. In this report, we will work more closely with one of these applications, namely face recognition. Face recognition is perhaps one of the most studied problems in machine learning and statistical classification. Possibly because the traditional approaches still show several weaknesses, as we will discuss more in Section 4, but also because of the human mind's extraordinary ability to recognize faces, even when key elements such as skin tone, hair color or length, facial hair or glasses, change. </p>

<p>In this report we will first give a general introduction to the basic concepts of compressive sensing, and then look at how these techniques can be applied to the face recognition problem. </p>


<h3>Preliminaries and Notation</h3>
<p>In this section we will introduce some of the necessary notation and concepts for this report. </p>

<p>Throughout we will denote vectors by boldface lower case letters, and matrices by boldface upper case letters. For a vector $ \mathbf{x} $, we will by $ x_{i} $ refer to the $ i $'th element in the vector. Similarly, for a matrix $ \mathbf{A} $, we will by $ \mathbf{a}_{i} $ refer to the $ i $'th  column of $ \mathbf{A} $, and by $ a_{i, j} $ denote the element of $ \mathbf{A} $ at column $ j $ and row $ i $.</p>

<p>Sets will be denoted by italic upper case letters, and the cardinality of a set $ S $ is denoted as $ |S| $. The complement of a set $ S $ will be written as $ \overline{S} $. </p>

<h4>Norms</h4>
<p>As we soon will see, a central part of compressive sensing is a minimization problem involving different norms of a vector. Therefore, we will begin with the definition of norms. Even though the author suspect this to be known material, it is included for ease of reference later.</p>

<p><b>Definition</b>
	Let $ V $ be a vector space over a field $ \mathbb{K} $.  A norm $ \lVert \cdot \rVert $ on $ V $ is a function $ \lVert \cdot \rVert\colon V \to\mathbb{R} $ such that
	<ol>
		<li> $ \lVert \mathbf{v} \rVert \geq 0 $ for all $ \mathbf{v} \in V $ with $ \lVert \mathbf{v} \rVert = \mathbf{0} $ if and only if $ \mathbf{v} = \mathbf{0} $ </li>
		<li> $ \lVert c\mathbf{v} \rVert = |c|\lVert \mathbf{v} \rVert $ for all $ \mathbf{v} \in V $ and $ c \in \mathbb{K} $ </li>
		<li> $ \lVert \mathbf{u} + \mathbf{v} \leq \lVert \mathbf{u} \rVert + \lVert \mathbf{v} \rVert $ for all $ \mathbf{u},  \mathbf{v} \in V $ </li>
	</ol>
</p>

<p>This definition is quite broad, and very general. In this report we will be working more closely with a family of norms called the $ \ell_{p} $ norms:</p>

<p><b>Definition</b> 
	Let $ p \geq 1 $ and $ p \in \mathbb{R} $. The $ \ell_{p} $ norm $ \lVert \cdot \rVert_{p} $ is defined as
	\[
		\lVert \mathbf{v} \rVert_{p} = \left( \sum_{i=1}^{n} |v_{i}|^{p} \right)^{\dfrac{1}{p}}
	\]
</p>

<p>We will not prove here that the $ \ell_{p} $-norms actually are norms, but this can be proven for all $ p\geq 1 $. We observe that for $ p=1 $ we get the Manhattan norm, and for $ p=2 $ we get euclidean norm. If we let $ p\to\infty $ we arrive at the supremum norm. Even though the above definition does not allow for $ p < 1 $, it can be proven that if we let $ p\to0 $, accept that $ 0^{0} = 0 $, and ignore the $ 1/p $-exponent, we get what is sometimes called the $ \ell_{0} $ norm:</p>

<p><b>Definition</b>
	The $ \ell_{0} $ norm $ \lVert \cdot \rVert_{0} $ is the number of non-zero entries in $ \mathbf{v} $.
</p>

<p>It is worth noting that the $ \ell_{0} $ norm is strictly speaking not a norm, since $ \lVert \cdot \rVert_{0} $ does not fulfill axiom \textit{(ii)} of \cref{def:norm}. In fact, for all $ q&lt;1 $, $ \lVert \cdot \rVert_{q} $ is not a norm, as $ \lVert \cdot \rVert_{q} $ does not fulfill axiom \textit{(iii)} for any $ q \in (0, 1) $. Despite this, it is customary to refer to $ \lVert \cdot \rVert_{0} $ as the $ \ell_{0} $ norm.</p>

<h4>Support and Sparsity</h4>
<p>As compressive sensing deals with the recovery of sparse vectors, we will need to define sparsity. Before we do that, we will introduce <i>support</i>:</p>

<p><b>Definition</b>
	Let $ \mathbf{v} \in \mathbb{C}^{N} $. The support $ S $ of $ \mathbf{v} $ is defined as the index set of its non-zero entries, that is:
	\[
		\text{supp}~ \mathbf{v} = \{j \in \{1, 2, ..., N\} \mid v_{j} \neq 0\}
	\]
</p>

<p>The notion of support yields a new formulation of the $ \ell_{0} $ norm: it is simply the cardinality of the support: $ \lVert \mathbf{v} \rVert_{0} = |\text{supp}~ \mathbf{v} | $.</p>

<p>For a vector $ \mathbf{v}\in\mathbb{C}^{N} $ and a set $ S \subset \{1, 2, ..., N\} $, we denote by $ \mathbf{v}_{S} $ either the subvector in $ \mathbb{C}^{|S|} $ consisting of the entries in $ \mathbf{v} $ indexed by $ S $, that is:</p>

\[
	\label{eq:vSalt1}
	(\mathbf{v}_{S})_{i} = v_{i} ~\text{for}~ i \in S
\]

<p>
Or the vector in $ \mathbb{C}^{N} $ which coincides with $ \mathbf{v} $ on the indices in $ S $, and is zero otherwise, that is:</p>

\[
	\label{eq:vSalt2}
	(\mathbf{v}_{S})_{i} = v_{i} \text{ if } i \in S, 0 \text{ otherwise}
\]

<p>It should always be clear from context which of these is used. Similarly, for a matrix $ \mathbf{A}\in\mathbb{R}^{m \times n} $ we will by $ \mathbf{A}_{S} \in \mathbb{R}^{m \times |S|} $ refer to the matrix consisting of the columns of $ \mathbf{A} $ indexed by $ S $.</p>

<p>The final concept we will introduce is the notion of <i>sparsity</i>:</p>

<p><b>Definition</b> A vector $ \mathbf{v} \in \mathbb{C}^{N} $ is said to be $ s $-sparse if it has no more than $ s $ non-zero entries. That is, $ \lVert \mathbf{v} \rVert_{0} \leq s $
</p>

<p>
Note that any vector supported on a set $ S $ with $ |S| = s $ is $ s $-sparse. 
</p>