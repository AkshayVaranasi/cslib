// Mathias Lohne, 2017

function updateMenuStyle() {
	// Get elements from HTML
	var menu = document.getElementById("topMenu");
	var menuFiller = document.getElementById("menuFiller");
	var body = document.getElementsByTagName("body")[0];

	// Update class of menu to be either fixed or relative.
	if (body.scrollTop > 90) {
		menu.className = "topMenuFixed";
		menuFiller.style = "height: 50px;";

	} else {
		menu.className = "topMenuMoving";
		menuFiller.style = "height: 0px;";
	}
}


function selectMenuItem(index) {
	var items = document.getElementsByClassName("topMenuElement");

	for (var i=0; i<items.length; i++) {
		items[i].style = "";
	}

	items[index].style = "background-color: #e2e2e2;";
}


function autoSelectMenuItem(pageName) {
	switch(pageName) {
		case "main":
			selectMenuItem(0);
			break;

		case "code":
			selectMenuItem(1);
			break;

		case "examples":
			selectMenuItem(2);
			break;

		case "chapters":
			selectMenuItem(3);
			break;
	}
}