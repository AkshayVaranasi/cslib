// Mathias Lohne, 2017

var currentChapter = -1;
var totalNumOfChapters = 3;

function createRequest() {
	var request;

	if (window.XMLHttpRequest) {
		// Works for IE7+, Firefox, Chrome, Opera, Safari
		request = new XMLHttpRequest();

	} else {
		// Legacy for IE6, IE5
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}

	return request;
}

function printError(error) {
	document.getElementById("section").innerHTML="<p>"+error+"</p>";
}

function loadContent(page) {
	var request = createRequest();

	// make sure current chapter is set
	if (page.substr(0, 9) == "chapters/") {
		currentChapter = parseInt(page.substr(16));

	} else {
		currentChapter = -1;
	}

	request.onreadystatechange = function() {
		if (request.readyState==4) {

			switch(request.status) {
				case 200:
					// Add content to section div
					document.getElementById("section").innerHTML = request.responseText;

					// Make sure math is rendered correctly
					MathJax.Hub.Queue(["Typeset",MathJax.Hub]);

					// Make sure code is rendered correctly
					var codeBlocks = document.getElementsByTagName("pre");
					for (var i = 0; i < codeBlocks.length; i++) {
						hljs.highlightBlock(codeBlocks[i]);
					}

					// Add chapter navigation bar if neccesary
					if (currentChapter != -1) fixChapterMenu();

					// Add click listener to all images
					imgzm.applyToAllImgTags();
					
					break;

				case 404:
					printError("404 : The page you are looking for doesn't exist");
					break;

				default:
					printError("Request error, error code: "+request.status);
					break;
			}
		}
	}

	request.open("GET", page + ".html",true);
	request.setRequestHeader("Cache-Control", "no-cache, no-store, must-revalidate");
	request.setRequestHeader("Pragma", "no-cache");
	request.setRequestHeader("Expires", "0");
	request.send();
}

function getQuaryValue(query, key) {
	var parts = query.split('?');

	for(var i = 0; i < parts.length; i++) {
		var part = parts[i];
		var sep = part.indexOf('=');

		if(sep == -1) continue;

		var tkey = decodeURIComponent(part.substr(0, sep)).trim();

		if(tkey == key) return part.substr(sep + 1).trim();
	}

	return "";
}

function loadFromURL() {
	// Get page name from url
	var page = getQuaryValue(location.search.substr(1), "page");

	// Check that it's a valid name
	var re = new RegExp("^[a-zA-Z0-9/]+$");
	if(!re.test(page)) {
	    page="main";
	}

	// Load page
	loadContent(page);
	autoSelectMenuItem(page);
}

function loadDynamically(page) {
	// Check for same page, dont reload if were're allready on the requested page
	if (page == getQuaryValue(location.search.substr(1), "page")) return;

	// Change url in browser, and update browser history
	// window.location.assign(window.location.href.split("?")[0] + "?page=" + page);
	history.pushState(page, "Compressed Sensing", "?page=" + page);

	window.onpopstate = function(e){
		if(e.state){
			loadContent(e.state);
			autoSelectMenuItem(e.state);
		} else {
			loadFromURL();
		}
	};

	// Load page
	loadContent(page);
	autoSelectMenuItem(page);
}

function loadChapter(chapterNum) {
	// Load page
	loadDynamically("chapters/chapter" + chapterNum);
}

function fixChapterMenu() {
	// Setup navigation bar in top of chapters
	var chapterMenu = document.getElementById("chapterMenu");
	if (currentChapter == 1) {
		chapterMenu.innerHTML = "<a onclick=\"nextChapter()\" id=\"chapterMenuNext\">Chapter 2 &rarr;</a>";

	} else if (currentChapter == totalNumOfChapters) {
		chapterMenu.innerHTML = "<a onclick=\"prevChapter()\" id=\"chapterMenuPrev\">&larr; Chapter " + (currentChapter-1) + "</a>";

	} else {
		chapterMenu.innerHTML = "<a onclick=\"prevChapter()\" id=\"chapterMenuPrev\">&larr; Chapter " + (currentChapter-1) + "</a><a onclick=\"nextChapter()\" id=\"chapterMenuNext\">Chapter " + (currentChapter+1) + " &rarr;</a>";
	}
}

function nextChapter() {
	loadChapter(currentChapter+1);
}

function prevChapter() {
	loadChapter(currentChapter-1);
}